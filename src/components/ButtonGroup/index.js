import './button-group.css'

import {h} from 'hyperapp'

export default (title, ...children) =>
	h('div', {
		class:`button-group ${title.replace(/\s/g, '').toLowerCase()}`
	},
		h('div', { class: 'title' }, title),
		...children
	)
