import './plot-hook-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'plot-hook-input'
	},
		h('div', {}, `Objective: ${generatedContent.objective}`),
		h('div', {}, `Adversaries: ${generatedContent.adversaries}`),
		h('div', {}, `Rewards: ${generatedContent.rewards}`),
	)

export default (state, actions) =>
	h('div', {
		class:'plot-hook-overlay',
	},
		h('div', { class:'overlay-title'}, 'Generate a Plot Hook'),
		h('div', {
			class:`plot-hook-generator`
		},
			h('div', {
				class:'plot-hook-input'
			},
				state.overlay?.content?.objective ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
				h('div', {},
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GeneratePlotHook,
					}, 'generate plot hook')
				)
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.objective ? 'open' : 'close'}`
			},
				h('div', { class:'plot-hook-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add plot hook' }`),
		)
	)
