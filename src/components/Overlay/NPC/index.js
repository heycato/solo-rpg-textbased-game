import './npc-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'npc-input'
	},
		h('div', {}, `Identity: ${generatedContent.identity}`),
		h('div', {}, `Goal: ${generatedContent.goal}`),
		h('div', {}, `Features: ${generatedContent.features}`),
		h('div', {}, `Attitude to PCs: ${generatedContent.attitude}`),
		h('div', {}, `Converstation: ${generatedContent.conversation}`),
	)

export default (state, actions) =>
	h('div', {
		class:'npc-overlay',
	},
		h('div', { class:'overlay-title'}, 'Generate an NPC'),
		h('div', {
			class:`npc-generator`
		},
			h('div', {
				class:'npc-input'
			},
				state.overlay?.content?.identity ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
				h('div', {},
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GenerateNPC,
					}, 'generate npc')
				)
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.identity ? 'open' : 'close'}`
			},
				h('div', { class:'npc-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add npc' }`),
		)
	)
