import './save-overlay.css'

import {h} from 'hyperapp'

export default (state, actions) =>
	h('div', {
		class:'save-overlay',
	},
		h('div', { class:'overlay-title'}, 'Save Game'),
		h('div', { class:'save-input'},
			h('div', { class:'overlay-label'}, 'Enter a name for your game'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content: {
							gameName:e.target.innerText
						}
					}
				})
			}, state.game ? state.game : '')
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: actions.SaveGame
			}, 'save game'),
		)
	)
