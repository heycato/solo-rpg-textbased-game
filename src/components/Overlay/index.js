import './overlay.css'

import {h} from 'hyperapp'

export default (state, actions, content) =>
	h('div', {
		class:content ? 'overlay' : 'overlay hide',
		onclick: actions.CloseOverlay
	},
		h('div', {
			class:'overlay-content'
		}, content)
	)
