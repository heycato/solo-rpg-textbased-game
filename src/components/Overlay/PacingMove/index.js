import './pacing-move-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'pacing-move-input'
	},
		h('div', {}, `Move: ${generatedContent.move}`),
	)

export default (state, actions) =>
	h('div', {
		class:'pacing-move-overlay',
	},
		h('div', { class:'overlay-title'}, 'What happens next?'),
		h('div', {
			class:`pacing-move-generator`
		},
			h('div', {
				class:'pacing-move-input'
			},
				state.overlay?.content?.move ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GeneratePacingMove
					}, 'generate prompt')
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.move ? 'open' : 'close'}`
			},
				h('div', { class:'pacing-move-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.result : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add move' }`),
		)
	)
