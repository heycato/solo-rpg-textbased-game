import './load-game-overlay.css'

import {h} from 'hyperapp'

export default (state, actions) =>
	h('div', {
		class:'load-game-overlay',
	},
		h('div', { class:'overlay-title'}, 'Load A Saved Game'),
		h('div', { class:'load-game-input'},
			h('div', { class:'overlay-label'}, 'Choose which game you want to continue playing.'),
			...JSON.parse(localStorage.games)
				.map(game =>
					h('button', {
						class: `load-game-button ${state.overlay.content?.game?.name === game.name ? 'load-game-selected' : ''}`,
						onclick: () => actions.SelectGame(game)
					}, game.name)
			)
		),
		h('div', { class:'load-game-input'},
			h('div', { class:'overlay-label'}, 'Or load from file:'),
			h('input', {
				class:'textarea',
				type:'file',
				accept:'.json',
				onchange: e => actions.UploadFile(e.target.files[0])
			})
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'delete-button',
				disabled:!state.overlay.content.game,
				onclick: actions.DeleteGame
			}, 'delete game'),
			h('button', {
				class:'apply-button',
				onclick: actions.LoadGame,
				disabled:!state.overlay.content.game,
			}, 'load game'),
		)
	)
