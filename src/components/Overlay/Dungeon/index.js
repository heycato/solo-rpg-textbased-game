import './dungeon-overlay.css'

import {h} from 'hyperapp'

import RadioGroup from '../../RadioGroup/index.js'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'dungeon-input'
	},
		...Object.entries(generatedContent)
			.filter(([key, value]) => !!value)
			.map(([key, value]) =>
				h('div', {}, `${key.charAt(0).toUpperCase() + key.slice(1)}: ${value}`))
	)

export default (state, actions) =>
	h('div', {
		class:'dungeon-overlay',
	},
		h('div', { class:'overlay-title'}, 'Generate a Dungeon or Structure'),
		h('div', {
			class:`dungeon-generator`
		},
			h('div', {
				class:'dungeon-input'
			},
				state.overlay?.content?.location ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
				h('div', {},
					RadioGroup(state.overlay.content.type, ['Room', 'Entrance'], actions.SetDungeonType),
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GenerateDungeon,
					}, 'generate dungeon')
				)
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.location ? 'open' : 'close'}`
			},
				h('div', { class:'dungeon-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add dungeon' }`),
		)
	)
