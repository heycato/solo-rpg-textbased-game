import './random-event-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'random-event-input'
	},
		h('div', {}, `Action: ${generatedContent.event.action[0]}, ${generatedContent.event.action[1]}`),
		h('div', {}, `Topic: ${generatedContent.event.topic[0]}, ${generatedContent.event.topic[1]}`),
	)

export default (state, actions) =>
	h('div', {
		class:'random-event-overlay',
	},
		h('div', { class:'overlay-title'}, 'Generate a Random Event'),
		h('div', {
			class:`random-event-generator`
		},
			h('div', {
				class:'random-event-input'
			},
				state.overlay?.content?.event ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GenerateRandomEvent
					}, 'generate event')
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.event ? 'open' : 'close'}`
			},
				h('div', { class:'random-event-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.result : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add event' }`),
		)
	)
