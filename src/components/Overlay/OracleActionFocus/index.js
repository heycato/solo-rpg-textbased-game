import './oracle-action-focus-overlay.css'

import {h} from 'hyperapp'

import RadioGroup from '../../RadioGroup/index.js'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'oracle-action-focus-input'
	},
		h('div', {}, `Answer: ${generatedContent.answer}`),
	)

export default (state, actions) =>
	h('div', {
		class:'oracle-action-focus-overlay',
	},
		h('div', { class:'overlay-title'}, 'Ask a "What does it do?" Question'),
		h('div', { class:'oracle-action-focus-input'},
			h('div', { class:'overlay-label'}, 'Ask your question:'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							question:e.target.innerText
						}
					},
					isEdit: state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.question : '')
		),
		h('div', {
			class:`oracle-action-focus-generator`
		},
			h('div', {
				class:'oracle-action-focus-input'
			},
				state.overlay?.content?.answer ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
				h('div', {},
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.AskActionFocus,
					}, 'ask question')
				)
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.answer ? 'open' : 'close'}`
			},
				h('div', { class:'oracle-action-focus-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
		)
	)
