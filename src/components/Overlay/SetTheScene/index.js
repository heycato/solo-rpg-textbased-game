import './set-the-scene-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'set-the-scene-input'
	},
		h('div', {}, `Complication: ${generatedContent.complication}`),
		h('div', {}, `Alteration: ${generatedContent.alteration}`),
	)

export default (state, actions) =>
	h('div', {
		class:'set-the-scene-overlay',
	},
		h('div', { class:'overlay-title'}, 'Set Up The Scene'),
		h('div', { class:'set-the-scene-input'},
			h('div', { class:'overlay-label'}, 'How does the scene start? What is your character doing and hope to accomplish?'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							scene:e.target.innerText
						}
					},
					isEdit: state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.scene : '')
		),
		h('div', {
			class:`set-the-scene-generator`
		},
			h('div', {
				class:'set-the-scene-input'
			},
				state.overlay?.content?.complication ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GenerateScene
					}, 'generate complications')
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.complication ? 'open' : 'close'}`
			},
				h('div', { class:'set-the-scene-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.result : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add scene' }`),
		)
	)
