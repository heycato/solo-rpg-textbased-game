import './player-action-overlay.css'

import {h} from 'hyperapp'

export default (state, actions) =>
	h('div', {
		class:'player-action-overlay',
	},
		h('div', { class:'overlay-title'}, 'Add a Player Action'),
		h('div', { class:'player-action-input'},
			h('div', { class:'overlay-label'}, 'What did they do/attempt? (skill check, or action)'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							action:e.target.innerText
						}
					},
					isEdit: state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.action : '')
		),
		h('div', {
			class:'dice-side-input'
		},
			h('div', {
				class:'fate-dice-container',
				onclick:actions.RollFate
			},
				...state.overlay.content.roll
					.map(([sym]) =>
						h('div', {class:'fate-die-frame'},
							h('div', {class:'fate-die-face'},
								h('div', {class:'fate-die-symbol'}, sym)
							)
						)
					),
				h('div', {
					class: `fate-roll-result ${state.overlay.content.result >= 0 ? 'pos' : 'neg'}`
				}, state.overlay.content.result > 0 ? `+${state.overlay.content.result}` : state.overlay.content.result),
			),
		),
		h('div', { class:'player-action-input'},
			h('div', { class:'overlay-label'}, 'What was the result? (skill roll, pass/fail)'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							outcome:e.target.innerText
						}
					},
					isEdit:state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.outcome : '')
		),
		h('div', { class:'player-action-input'},
			h('div', { class:'overlay-label'}, 'Describe what happened: (tell the story)'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							description:e.target.innerText
						}
					},
					isEdit:state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.description : '')
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add action' }`),
		)
	)
