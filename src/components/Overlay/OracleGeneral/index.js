import './oracle-general-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'oracle-general-input'
	},
		h('div', {}, `What it does: ${generatedContent.operation}`),
		h('div', {}, `How it looks: ${generatedContent.appearance}`),
		h('div', {}, `Significance: ${generatedContent.significance}`),
	)

export default (state, actions) =>
	h('div', {
		class:'oracle-general-overlay',
	},
		h('div', { class:'overlay-title'}, 'Ask a vague or general question'),
		h('div', { class:'oracle-general-input'},
			h('div', { class:'overlay-label'}, 'Ask your question:'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							question:e.target.innerText
						}
					},
					isEdit: state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.question : '')
		),
		h('div', {
			class:`oracle-general-generator`
		},
			h('div', {
				class:'oracle-general-input'
			},
				state.overlay?.content?.operation ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
				h('div', {},
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.AskGeneralQuestion,
					}, 'ask question')
				)
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.operation ? 'open' : 'close'}`
			},
				h('div', { class:'oracle-general-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
		)
	)
