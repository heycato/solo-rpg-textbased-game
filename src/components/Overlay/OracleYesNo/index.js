import './oracle-yesno-overlay.css'

import {h} from 'hyperapp'

import RadioGroup from '../../RadioGroup/index.js'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'oracle-yesno-input'
	},
		h('div', {}, `Answer: ${generatedContent.answer}`),
	)

export default (state, actions) =>
	h('div', {
		class:'oracle-yesno-overlay',
	},
		h('div', { class:'overlay-title'}, 'Ask a Yes or No Question'),
		h('div', { class:'oracle-yesno-input'},
			h('div', { class:'overlay-label'}, 'Ask your question:'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							question:e.target.innerText
						}
					},
					isEdit: state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.question : '')
		),
		h('div', {
			class:`oracle-yesno-generator`
		},
			h('div', {
				class:'oracle-yesno-input'
			},
				state.overlay?.content?.answer ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
				h('div', {},
					RadioGroup(state.overlay.content.odds, Object.keys(state.model.ORACLE_YESNO), actions.SetOdds),
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.AskYesNo,
						disabled: !state.overlay.content.odds
					}, 'ask question')
				)
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.answer ? 'open' : 'close'}`
			},
				h('div', { class:'oracle-yesno-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
		)
	)
