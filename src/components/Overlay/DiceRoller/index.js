import './dice-roller-overlay.css'

import {h} from 'hyperapp'

import RadioGroup from '../../RadioGroup/index.js'
import {randFromArr} from '../../../utils/index.js'

const buildMultiSidedDiceRoller = (state, actions) =>
	h('div', {
		class:'dice-side-input'
	},
		h('div', {}, 'Sides:'),
		h('div', {
			contenteditable:true,
		})
	)

const buildFateDiceRoller = (state, actions) =>
	h('div', {
		class:'dice-side-input'
	},
		h('div', {
			class:'fate-dice-container'
		},
			...state.overlay.content.roll
				.map(([sym]) =>
					h('div', {class:'fate-die-frame'},
						h('div', {class:'fate-die-face'},
							h('div', {class:'fate-die-symbol'}, sym)
						)
					)
				),
			h('div', {
				class: 'fate-roll-result'
			}, state.overlay.content.result > 0 ? `+${state.overlay.content.result}` : state.overlay.content.result),
		),
	)

export default (state, actions) =>
	h('div', {
		class:'dice-roller-overlay',
	},
		h('div', { class:'overlay-title'}, 'Fate Dice Roller'),
		// RadioGroup(state.overlay.content.type, ['Multi-sided', 'Fate'], actions.SelectDiceType),
		// h('div', {
		// 	class:'dice-roller-container'
		// },
		// 	(state.overlay.content.type === 'Multi-sided' ?  buildMultiSidedDiceRoller(state, actions) :
		// 		state.overlay.content.type === 'Fate' ? buildFateDiceRoller(state, actions) : '')
		// ),
		buildFateDiceRoller(state, actions),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'close'),
			h('button', {
				class:'apply-button',
				onclick:actions.RollFate
			}, 'roll'),
		)
	)
