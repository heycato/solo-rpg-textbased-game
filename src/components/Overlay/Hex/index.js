import './hex-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'hex-input'
	},
		...Object.entries(generatedContent)
			.map(([key, value]) =>
				h('div', {}, `${key.charAt(0).toUpperCase() + key.slice(1)}: ${value}`))
	)

export default (state, actions) =>
	h('div', {
		class:'hex-overlay',
	},
		h('div', { class:'overlay-title'}, 'Generate an environment'),
		h('div', {
			class:`hex-generator`
		},
			h('div', {
				class:'hex-input'
			},
				state.overlay?.content?.terrain ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
				h('div', {},
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GenerateHex,
					}, 'generate environment')
				)
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.terrain ? 'open' : 'close'}`
			},
				h('div', { class:'hex-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add hex' }`),
		)
	)
