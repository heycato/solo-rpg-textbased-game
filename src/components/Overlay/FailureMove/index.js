import './failure-move-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'failure-move-input'
	},
		h('div', {}, `Move: ${generatedContent.move}`),
	)

export default (state, actions) =>
	h('div', {
		class:'failure-move-overlay',
	},
		h('div', { class:'overlay-title'}, 'Add A Failure Consequence'),
		h('div', {
			class:`failure-move-generator`
		},
			h('div', {
				class:'failure-move-input'
			},
				state.overlay?.content?.move ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GenerateFailureMove
					}, 'generate consequence')
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.move ? 'open' : 'close'}`
			},
				h('div', { class:'failure-move-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.result : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add move' }`),
		)
	)
