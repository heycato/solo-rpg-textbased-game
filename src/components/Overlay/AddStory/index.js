import './add-story-overlay.css'

import {h} from 'hyperapp'

export default (state, actions) =>
	h('div', {
		class:'add-story-overlay',
	},
		h('div', { class:'overlay-title'}, 'Add A Story'),
		h('div', { class:'add-story-input'},
			h('div', { class:'overlay-label'}, 'Describe what happened:'),
			h('div', {
				class:'text-area',
				contenteditable:true,
				onblur: e => actions.UpdateOverlayContent({
					content: {
						...state.overlay,
						content:{
							...state.overlay.content,
							description:e.target.innerText
						}
					},
					isEdit:state.isEdit
				})
			}, state.isEdit ? state.overlay?.content?.description : '')
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add story' }`),
		)
	)
