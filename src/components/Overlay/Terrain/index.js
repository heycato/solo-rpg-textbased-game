import './terrain-overlay.css'

import {h} from 'hyperapp'

const buildGeneratedDisplay = generatedContent =>
	h('div', {
		class:'terrain-input'
	},
		h('div', {}, `Terrain: ${generatedContent.terrain}`),
	)

export default (state, actions) =>
	h('div', {
		class:'terrain-overlay',
	},
		h('div', { class:'overlay-title'}, 'Generate Terrain'),
		h('div', {
			class:`terrain-generator`
		},
			h('div', {
				class:'terrain-input'
			},
				state.overlay?.content?.terrain ?
					h('div', {
						class: 'overlay-generate-display'
					}, buildGeneratedDisplay(state.overlay.content)) :
					h('button', {
						class: 'overlay-generate-button',
						onclick: actions.GenerateTerrain
					}, 'generate terrain')
			),
			h('div', {
				class:`collapse-container ${state.overlay?.content?.terrain ? 'open' : 'close'}`
			},
				h('div', { class:'terrain-input'},
					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
					h('div', {
						class:'text-area',
						contenteditable:true,
						onblur: e => actions.UpdateOverlayContent({
							content: {
								...state.overlay,
								content:{
									...state.overlay.content,
									description:e.target.innerText
								}
							},
							isEdit:state.isEdit
						})
					}, state.isEdit ? state.overlay?.content?.description : '')
				)
			)
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
			}, `${state.isEdit ? 'apply edits' : 'add terrain' }`),
		)
	)
