import './new-game-overlay.css'

import {h} from 'hyperapp'

export default (state, actions) =>
	h('div', {
		class:'new-game-overlay',
	},
		h('div', { class:'overlay-title'}, 'Create a New Game'),
		h('div', { class:'new-game-input'},
			h('div', { class:'overlay-label'}, 'Be sure to save the current game before continuing. Unsaved changes will be lost.'),
		),
		h('div', {
			class:'overlay-button-bar'
		},
			h('button', {
				class:'cancel-button',
				onclick: actions.CloseOverlay
			}, 'cancel'),
			h('button', {
				class:'apply-button',
				onclick: actions.NewGame
			}, 'continue'),
		)
	)
