import './radio-group.css'

import {h} from 'hyperapp'

const buildRadioButton = (selected, action) => value =>
	h('div', {
		class:'radio-button',
		onclick: () => action(value)
	},
		h('div', {
			class:`radio-indicator ${selected === value ? 'selected' : ''}`
		}),
		h('div', {
			class:'radio-label'
		}, value),
	)

export default (selected, options, action) =>
	h('div', {
		class:'radio-group'
	},
		...options.map(buildRadioButton(selected, action))
	)
