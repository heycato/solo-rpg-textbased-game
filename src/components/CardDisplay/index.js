import './card-display.css'

import {h} from 'hyperapp'
import Card from '../Card/index.js'

export default (state, actions) =>
	h('div', {
		class:`card-display ${state.overlay ? 'blur' : ''}`,
	},
		h('div', { class:'card-display-title', key:'card-display-title'}, state.game ? state.game : 'Untitled Game'),
		h('div', { class:'card-scroller'},
			...state.cards.map(Card(actions))
		)
	)
