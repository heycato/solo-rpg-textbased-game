import './control-panel.css'

import {h} from 'hyperapp'
import ControlButton from '../ControlButton/index.js'
import ButtonGroup from '../ButtonGroup/index.js'

export default (state, actions) =>
	h('div', {
		class:`control-panel ${state.overlay ? 'blur' : ''}`,
		onclick: actions.ButtonClick
	},
		ButtonGroup(
			'Actions',
			ControlButton('Add a Story', 'ADD_STORY', '`'),
			ControlButton('Add a Player Action', 'PLAYER_ACTION', '1'),
			ControlButton('Set up the Scene', 'SET_THE_SCENE', '2'),
			ControlButton('What next?', 'PACING_MOVE', '3'),
			ControlButton('Add a failure', 'FAILURE_MOVE', '4'),
		),
		ButtonGroup(
			'Ask The GM',
			ControlButton('Ask Yes or No?', 'ORACLE_YESNO', '6'),
			ControlButton('Ask quantity or quality', 'ORACLE_HOW', '7'),
			ControlButton('What does it do?', 'ACTION_FOCUS', '8'),
			ControlButton('What kind of thing?', 'DETAIL_FOCUS', '9'),
			ControlButton('What is this about?', 'TOPIC_FOCUS', '!'),
			ControlButton('General Questions', 'GENERAL', '@'),
		),
		ButtonGroup(
			'Generators',
			ControlButton('Random Event',  'RANDOM_EVENT', '5'),
			ControlButton('Plot Hook', 'PLOT_HOOK', '#'),
			ControlButton('NPC', 'NPC', '$'),
			ControlButton('Dungeon or Structure', 'DUNGEON', '%'),
			ControlButton('Environment', 'HEX', '^'),
			ControlButton('Environment Features', 'FEATURES', '&'),
			ControlButton('Terrain', 'TERRAIN', '*'),
		),
		ButtonGroup(
			'Utilities',
			ControlButton('New Game', 'NEW_GAME', 'n'),
			ControlButton('Save', 'SAVE', 's'),
			ControlButton('Load Game', 'LOAD_GAME', 'l'),
			h('button', {
				title:'Download (ctrl + d)',
				class:'control-button',
				onclick:actions.ExportFile
			}, 'Download Current Game'),
			// ControlButton('Dice Roller', 'DICE_ROLLER', ' '),
		)

	)
