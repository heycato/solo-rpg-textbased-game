import './control-button.css'

import {h} from 'hyperapp'

export default (label, action, command) =>
	h('button', {
		title:`${label} (ctrl + ${command})`,
		class:'control-button',
		value:action
	},
		label
	)
