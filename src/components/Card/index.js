import './card.css'

import {h} from 'hyperapp'

const createContentItem = (label, value) =>
	h('div', {class:'card-item'},
		h('div', {class:'card-item-label'}, `${label}:`),
		h('div', {class:'card-item-value'}, value)
	)

const createEventContentItem = event =>
	(['Event']).concat(Object.entries(event)
		.map(([label, value]) => createContentItem(label, value.join(', '))))

export default actions => (card, id) =>
	h('div', {
		key:`card-id-${id}`,
		class:`card ${card.type}`
	},
		h('div', {
			class:'card-header'
		},
			h('div', {},
				h('button', {
					class:'card-edit-button',
					onclick: () => actions.UpdateOverlayContent({content:card, isEdit:true})
				}, 'edit'),
				h('button', {
					class:'card-remove-button',
					onclick: () => actions.RemoveCard(id)
				}, 'remove'),
			)
		),
		(card.content.scene || card.content.question ?
			h('div', {
				class:'card-scene'
			}, card.content.scene || card.content.question) : ''),
		h('div', {
			class:'card-content'
		},
			...Object.entries(card.content)
			.filter(([label]) => !/description|scene|question/.test(label))
				.map(([label, value]) =>
					label === 'event' ?
						createEventContentItem(value) :
						createContentItem(label, value)
				)
		),
		h('div', {
			class:'card-description'
		}, card.content.description)
	)
