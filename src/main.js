import './main.css'

if('serviceWorker' in navigator) {
	navigator.serviceWorker
		.register('sw.js')
		.then(() =>
			console.log('registered service worker')
		)
}

// hack to unregister service workers from safari
// navigator.serviceWorker.getRegistrations().then(function(registrations) {
//	for(let registration of registrations) {
//	console.log('unregistering', registration)
//	 registration.unregister()
// } })

if(!localStorage.games) localStorage.games = '[]'

import {app, h} from 'hyperapp'
import {withLogger} from '@hyperapp/logger'
import CardDisplay from './components/CardDisplay/index.js'
import ControlPanel from './components/ControlPanel/index.js'
import Overlay from './components/Overlay/index.js'
import AddStoryOverlay from './components/Overlay/AddStory/index.js'
import PlayerActionOverlay from './components/Overlay/PlayerAction/index.js'
import SetTheSceneOverlay from './components/Overlay/SetTheScene/index.js'
import PacingMoveOverlay from './components/Overlay/PacingMove/index.js'
import FailureMoveOverlay from './components/Overlay/FailureMove/index.js'
import RandomEventOverlay from './components/Overlay/RandomEvent/index.js'
import PlotHookOverlay from './components/Overlay/PlotHook/index.js'
import NPCOverlay from './components/Overlay/NPC/index.js'
import DungeonOverlay from './components/Overlay/Dungeon/index.js'
import HexOverlay from './components/Overlay/Hex/index.js'
import FeaturesOverlay from './components/Overlay/Features/index.js'
import TerrainOverlay from './components/Overlay/Terrain/index.js'
import DiceRollerOverlay from './components/Overlay/DiceRoller/index.js'
import OracleYesNoOverlay from './components/Overlay/OracleYesNo/index.js'
import OracleHowOverlay from './components/Overlay/OracleHow/index.js'
import OracleActionFocusOverlay from './components/Overlay/OracleActionFocus/index.js'
import OracleDetailFocusOverlay from './components/Overlay/OracleDetailFocus/index.js'
import OracleTopicFocusOverlay from './components/Overlay/OracleTopicFocus/index.js'
import OracleGeneralOverlay from './components/Overlay/OracleGeneral/index.js'
import NewGameOverlay from './components/Overlay/NewGame/index.js'
import SaveOverlay from './components/Overlay/Save/index.js'
import LoadGameOverlay from './components/Overlay/LoadGame/index.js'
import History from './utils/History.js'
import { createKeyCommandsExec, sum } from './utils/index.js'
import {
	generateFateRoll,
	generateScene,
	generatePacingMove,
	generateFailureMove,
	generateRandomEvent,
	generatePlotHook,
	generateNPC,
	generateDungeon,
	generateHex,
	generateFeatures,
	generateTerrain,
	displayRandomEvent,
	askYesNo,
	askHowMany,
	askActionFocus,
	askDetailFocus,
	askTopicFocus,
	askGeneralQuestion,
} from './utils/generators.js'

const history = new History()

const historyApp = withLogger({
	log(prevState, action, nextState) {
		const pStateStr = JSON.stringify(prevState)
		const nStateStr = JSON.stringify(nextState)
		if(action.name !== 'SetState' && pStateStr !== nStateStr) {
			// ignore SetState
			history.push(nextState)
		}
		/*
		console.group('State Change:')
		console.log('Previous State:', prevState)
		console.log('Action:', action)
		console.log('Next State:', nextState)
		console.groupEnd('State Change:')
		*/
	}
})(app)

const data = fetch('data.json')
	.then(r => r.json())

const actions = {
	SetState: newState => () =>
		({...newState}),
	ButtonClick: e => state => {
		const action = e.target.value
		if(action) {
			// console.log(action)
			switch(action) {
				case 'ADD_STORY':
					return ({
						...state,
						overlay:{
							command:'ADD_STORY',
							type:'actions',
							id:state.cards.length,
							content:{description:''}
						}
					})
				case 'PLAYER_ACTION':
					return ({
						...state,
						overlay:{
							command:'PLAYER_ACTION',
							type:'actions',
							id:state.cards.length,
							content:{action:'', outcome:'', result:'', roll:[[''],[''],[''],['']],  description:''}
						}
					})
				case 'SET_THE_SCENE':
					return ({
						...state,
						overlay:{
							command:'SET_THE_SCENE',
							type:'actions',
							id:state.cards.length,
							content:{scene:'', complication:'', alteration:'', description:''}
						}
					})
				case 'PACING_MOVE':
					return ({
						...state,
						overlay:{
							command:'PACING_MOVE',
							type:'actions',
							id:state.cards.length,
							content:{move:'', description:''}
						}
					})
				case 'FAILURE_MOVE':
					return ({
						...state,
						overlay:{
							command:'FAILURE_MOVE',
							type:'actions',
							id:state.cards.length,
							content:{move:'', description:''}
						}
					})
				case 'ORACLE_YESNO':
					return ({
						...state,
						overlay:{
							command:'ORACLE_YESNO',
							type:'oracles',
							id:state.cards.length,
							content:{question:'', odds:'', answer:'', description:''}
						}
					})
				case 'ORACLE_HOW':
					return ({
						...state,
						overlay:{
							command:'ORACLE_HOW',
							type:'oracles',
							id:state.cards.length,
							content:{question:'', answer:'', description:''}
						}
					})
				case 'ACTION_FOCUS':
					return ({
						...state,
						overlay:{
							command:'ACTION_FOCUS',
							type:'oracles',
							id:state.cards.length,
							content:{question:'', answer:'', description:''}
						}
					})
				case 'DETAIL_FOCUS':
					return ({
						...state,
						overlay:{
							command:'DETAIL_FOCUS',
							type:'oracles',
							id:state.cards.length,
							content:{question:'', answer:'', description:''}
						}
					})
				case 'TOPIC_FOCUS':
					return ({
						...state,
						overlay:{
							command:'TOPIC_FOCUS',
							type:'oracles',
							id:state.cards.length,
							content:{question:'', answer:'', description:''}
						}
					})
				case 'GENERAL':
					return ({
						...state,
						overlay:{
							command:'GENERAL',
							type:'oracles',
							id:state.cards.length,
							content:{question:'', operation:'', appearance:'', significance:'', description:''}
						}
					})
				case 'RANDOM_EVENT':
					return ({
						...state,
						overlay:{
							command:'RANDOM_EVENT',
							type:'generators',
							id:state.cards.length,
							content:{event:'', description:''}
						}
					})
				case 'PLOT_HOOK':
					return ({
						...state,
						overlay:{
							command:'PLOT_HOOK',
							type:'generators',
							id:state.cards.length,
							content:{objective:'', adversaries:'', rewards:'', description:''}
						}
					})
				case 'NPC':
					return ({
						...state,
						overlay:{
							command:'NPC',
							type:'generators',
							id:state.cards.length,
							content:{identity:'', goal:'', features:'', attitude:'', conversation:'', description:''}
						}
					})
				case 'DUNGEON':
					return ({
						...state,
						overlay:{
							command:'DUNGEON',
							type:'generators',
							id:state.cards.length,
							content:{
								type:'',
								location:'',
								encounter:'',
								object:'',
								exits:''
							}
						}
					})
				case 'HEX':
					return ({
						...state,
						overlay:{
							command:'HEX',
							type:'generators',
							id:state.cards.length,
							content:{
								terrain:'',
								contents:'',
								features:'',
								event:'',
							}
						}
					})
				case 'FEATURES':
					return ({
						...state,
						overlay:{
							command:'FEATURES',
							type:'generators',
							id:state.cards.length,
							content:{features:'', description:''}
						}
					})
				case 'TERRAIN':
					return ({
						...state,
						overlay:{
							command:'TERRAIN',
							type:'generators',
							id:state.cards.length,
							content:{terrain:'', description:''}
						}
					})
				case 'NEW_GAME':
					return ({
						...state,
						overlay:{
							command:'NEW_GAME',
							type:'utilities',
							id:state.cards.length,
						}
					})
				case 'SAVE':
					return ({
						...state,
						overlay:{
							command:'SAVE',
							type:'utilities',
							id:state.cards.length,
							content:{gameName:'', description:''}
						}
					})
				case 'LOAD_GAME':
					return ({
						...state,
						overlay:{
							command:'LOAD_GAME',
							type:'utilities',
							id:state.cards.length,
							content:{game:'', description:''}
						}
					})
				case 'EXPORT':
					return ({...state})
				case 'DICE_ROLLER':
					return ({
						...state,
						overlay:{
							command:'DICE_ROLLER',
							type:'utilities',
							id:state.cards.length,
							content:{type:'Fate', dice:'', quantity:'', roll:[], result:'', description:''}
						}
					})
				default:
					return ({
						...state,
						overlay:{
							command:'PLAYER_ACTION',
							type:'actions',
							id:state.cards.length,
							content:{action:'', result:'', description:''}
						}
					})
			}
		}
	},
	SelectDiceType: type => state =>
		({ ...state, overlay:{
			...state.overlay,
			content:{...state.overlay.content, type}}
		}),
	NewGame: () => state =>
		({...state, overlay:'', cards:[]}),
	SaveGame: () => state => {
		const games = JSON.parse(localStorage.getItem('games')) || []
		let editedGames
		if(state.game) {
			if(state.game !== state.overlay?.content?.gameName) {
				// save current game as...
				editedGames = games.concat({name:state.overlay?.content?.gameName || state.game, cards:state.cards})
			} else {
				editedGames = games
					.map(game => {
						return state.game === game.name ? ({name:state.overlay?.content?.gameName || state.game, cards:state.cards}) : game
					})
			}
		} else {
			// save new game
			editedGames = games.concat({name:state.overlay?.content?.gameName || state.game, cards:state.cards})
		}
		localStorage.setItem('games', JSON.stringify(editedGames))
		return ({...state, overlay:'', game:state.overlay?.content?.gameName || state.game})
	},
	LoadGame: () => (state, actions) =>
		({
			...state,
			overlay:'',
			game:state.overlay.content.game.name,
			cards:state.overlay.content.game.cards
		}),
	DeleteGame: () => (state) => {
		let games = JSON.parse(localStorage.getItem('games')) || []
		games = games.filter(({name}) => name !== state.overlay.content.game.name)
		localStorage.setItem('games', JSON.stringify(games))
		if(!games.find(game => game.name === state.game)) {
			return ({...state, overlay:'', game:'', cards:[]})
		} else {
			return ({...state, overlay:''})
		}
	},
	UploadFile: file => (state, actions) => {
		const reader = new FileReader()
		reader.addEventListener('load', e => {
			actions.SelectGame(JSON.parse(e.target.result))
		})
		reader.readAsText(file)
	},
	SelectGame: game => state =>
		({ ...state, overlay:{
			...state.overlay,
			content:{...state.overlay.content, game}}
		}),
	DownloadFile: () => state => {
		let dataStr = JSON.stringify({name:state.game, cards:state.cards})
		let dataUri = 'data:application/json;charset=utf-8,'+encodeURIComponent(dataStr)
		let exportFileDefaultName = `${state.game}.json`
		let el = document.createElement('a')
		el.setAttribute('href', dataUri)
		el.setAttribute('download', exportFileDefaultName)
		el.click()
	},
	RollFate: () => state => {
		const roll = generateFateRoll(state.model)
		const result = roll.map(([_, n]) => n).reduce(sum, 0)
		return ({
			...state,
			overlay:{
				...state.overlay,
				content: {
					...state.overlay.content,
					roll,
					result
				}
			},
		})
	},
	UpdateOverlayContent: data => state =>
		({...state, overlay:data.content, isEdit:data.isEdit}),
	AddCard: () => state =>
		({
			...state,
			overlay:'',
			cards:state.cards.concat({...state.overlay})
		}),
	EditCard: id => state =>
		({
			...state,
			isEdit:false,
			overlay:'',
			cards:[
				...state.cards.slice(0, id),
				state.overlay,
				...state.cards.slice(id + 1)]
		}),
	RemoveCard: id => state =>
		({
			...state,
			cards:[...state.cards.slice(0, id), ...state.cards.slice(id + 1)]
		}),
	CloseOverlay: e => state =>
		e.target === e.currentTarget ?
			({...state, overlay:''}) :
			undefined,
	GenerateScene:() => state => {
		const [complication, alteration] = generateScene(state.model)
		return ({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				complication,
				alteration
			}
		}})
	},
	GeneratePacingMove:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				move:generatePacingMove(state.model),
			}
		}}),
	GenerateFailureMove:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				move:generateFailureMove(state.model),
			}
		}}),
	GenerateRandomEvent:() => state => {
		const [actionFocus, topicFocus] = generateRandomEvent(state.model)
		return ({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				event:{
					action: actionFocus,
					topic: topicFocus,
				},
			}
		}})
	},
	GeneratePlotHook:() => state => {
		const [objective, adversaries, rewards] = generatePlotHook(state.model)
		return ({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				objective,
				adversaries,
				rewards
			}
		}})
	},
	GenerateNPC:() => state => {
		const [identity, goal, features, attitude, conversation] = generateNPC(state.model)
		return ({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				identity,
				goal,
				features,
				attitude,
				conversation
			}
		}})
	},
	SetDungeonType:type => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				type
			}
		}}),
	GenerateDungeon:() => state => {
		const [location, encounter, object, exits] = generateDungeon(state.model)
		const appendObj = { location, encounter, object, exits }
		if(state.overlay.content.type === 'Entrance') {
			appendObj.appearance = askDetailFocus(state.model).join(', ')
			appendObj.operation = askActionFocus(state.model).join(', ')
		}
		return ({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				...appendObj
			}
		}})
	},
	GenerateHex:() => state => {
		const [terrain, contents, features, event] = generateHex(state.model)
		return ({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				terrain,
				contents,
				features,
				event
			}
		}})
	},
	GenerateFeatures:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				features:generateFeatures(state.model),
			}
		}}),
	GenerateTerrain:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				terrain:generateTerrain(state.model),
			}
		}}),
	SetOdds:odds => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				odds,
			}
		}}),
	AskYesNo:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				answer:askYesNo(state.model, state.overlay.content.odds),
			}
		}}),
	AskHowMany:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				answer:askHowMany(state.model),
			}
		}}),
	AskActionFocus:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				answer:askActionFocus(state.model).join(', '),
			}
		}}),
	AskDetailFocus:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				answer:askDetailFocus(state.model).join(', '),
			}
		}}),
	AskTopicFocus:() => state =>
		({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				answer:askTopicFocus(state.model).join(', '),
			}
		}}),
	AskGeneralQuestion:() => state => {
		const [operation, appearance, significance] = askGeneralQuestion(state.model)
		return ({...state, overlay:{
			...state.overlay,
			content: {
				...state.overlay.content,
				operation: operation.join(', '),
				appearance: appearance.join(', '),
				significance
			}
		}})
	}
}

const initialState = {
	overlay:'',
	cards:[]
}

const getOverlayContent = (state, actions) => {
	switch(state.overlay.command) {
		case 'PLAYER_ACTION':
			return PlayerActionOverlay(state, actions)
		case 'ADD_STORY':
			return AddStoryOverlay(state, actions)
		case 'SET_THE_SCENE':
			return SetTheSceneOverlay(state, actions)
		case 'PACING_MOVE':
			return PacingMoveOverlay(state, actions)
		case 'FAILURE_MOVE':
			return FailureMoveOverlay(state, actions)
		case 'RANDOM_EVENT':
			return RandomEventOverlay(state, actions)
		case 'ORACLE_YESNO':
			return OracleYesNoOverlay(state, actions)
		case 'ORACLE_HOW':
			return OracleHowOverlay(state, actions)
		case 'ACTION_FOCUS':
			return OracleActionFocusOverlay(state, actions)
		case 'DETAIL_FOCUS':
			return OracleDetailFocusOverlay(state, actions)
		case 'TOPIC_FOCUS':
			return OracleTopicFocusOverlay(state, actions)
		case 'GENERAL':
			return OracleGeneralOverlay(state, actions)
		case 'PLOT_HOOK':
			return PlotHookOverlay(state, actions)
		case 'NPC':
			return NPCOverlay(state, actions)
		case 'DUNGEON':
			return DungeonOverlay(state, actions)
		case 'HEX':
			return HexOverlay(state, actions)
		case 'FEATURES':
			return FeaturesOverlay(state, actions)
		case 'TERRAIN':
			return TerrainOverlay(state, actions)
		case 'NEW_GAME':
			return NewGameOverlay(state, actions)
		case 'SAVE':
			return SaveOverlay(state, actions)
		case 'LOAD_GAME':
			return LoadGameOverlay(state, actions)
		case 'DICE_ROLLER':
			return DiceRollerOverlay(state, actions)
		default:
			return false
	}
}

const view = (state, actions) =>
	h('main', {},
		ControlPanel(state, actions),
		CardDisplay(state, actions),
		Overlay(state, actions, getOverlayContent(state, actions)),
	)

const init = (data, state) => () =>
	data
		.then(model => {
			const initState = {model, ...state}
			history.push(initState)
			const allActions = historyApp(initState, actions, view, document.body)
			window.addEventListener('keydown', createKeyCommandsExec(allActions, history))
		})
		.catch(e => {
			console.log('Error:', e)
		})

window.addEventListener('load', init(data, initialState))
