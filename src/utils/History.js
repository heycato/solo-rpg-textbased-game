import {
	clamp
} from './index.js'

export default class History {
	constructor(store=[]) {
		this.currentIndex = 0
		this.store = store
	}
	undo(n=1) {
		this.currentIndex = clamp(0, this.store.length - 1)(this.currentIndex - n)
		return this.store[this.currentIndex]
	}
	redo(n=1) {
		this.currentIndex = clamp(0, this.store.length - 1)(this.currentIndex + n)
		return this.store[this.currentIndex]
	}
	push(x) {
		this.store.push(x)
		this.currentIndex = this.store.length - 1
		return x
	}
	getCurrent() {
		return this.store[this.currentIndex]
	}
}
