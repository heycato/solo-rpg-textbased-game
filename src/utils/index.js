export const clamp = (min, max) => n =>
	n < min ? min :
		n > max ? max :
		n

export const createKeyCommandsExec = (actions, history) => e => {
	if(e.defaultPrevented) return
	if(e.ctrlKey) {
		switch(e.key) {
			case 'z': //undo
				let prevState = history.undo()
				actions.SetState(prevState)
				e.preventDefault()
				break
			case 'r': //redo
				let nextState = history.redo()
				actions.SetState(nextState)
				e.preventDefault()
				break
			case 'n':
				e.preventDefault()
				actions.ButtonClick({target:{value:'NEW_GAME'}})
				break
			case 's':
				e.preventDefault()
				actions.ButtonClick({target:{value:'SAVE'}})
				break
			case 'd':
				e.preventDefault()
				actions.DownloadFile()
				break
			case 'l':
				e.preventDefault()
				actions.ButtonClick({target:{value:'LOAD_GAME'}})
				break
			case ' ':
				e.preventDefault()
				actions.ButtonClick({target:{value:'DICE_ROLLER'}})
				break
			case '`':
				e.preventDefault()
				actions.ButtonClick({target:{value:'ADD_STORY'}})
				break
			case '1':
				e.preventDefault()
				actions.ButtonClick({target:{value:'PLAYER_ACTION'}})
				break
			case '2':
				e.preventDefault()
				actions.ButtonClick({target:{value:'SET_THE_SCENE'}})
				break
			case '3':
				e.preventDefault()
				actions.ButtonClick({target:{value:'PLACING_MOVE'}})
				break
			case '4':
				e.preventDefault()
				actions.ButtonClick({target:{value:'FAILURE_MOVE'}})
				break
			case '5':
				e.preventDefault()
				actions.ButtonClick({target:{value:'RANDOM_EVENT'}})
				break
			case '6':
				e.preventDefault()
				actions.ButtonClick({target:{value:'ORACLE_YESNO'}})
				break
			case '7':
				e.preventDefault()
				actions.ButtonClick({target:{value:'ORACLE_HOW'}})
				break
			case '8':
				e.preventDefault()
				actions.ButtonClick({target:{value:'ACTION_FOCUS'}})
				break
			case '9':
				e.preventDefault()
				actions.ButtonClick({target:{value:'DETAIL_FOCUS'}})
				break
			case '!':
				e.preventDefault()
				actions.ButtonClick({target:{value:'TOPIC_FOCUS'}})
				break
			case '@':
				e.preventDefault()
				actions.ButtonClick({target:{value:'GENERAL'}})
				break
			case '#':
				e.preventDefault()
				actions.ButtonClick({target:{value:'PLOT_HOOK'}})
				break
			case '$':
				e.preventDefault()
				actions.ButtonClick({target:{value:'NPC'}})
				break
			case '%':
				e.preventDefault()
				actions.ButtonClick({target:{value:'DUNGEON'}})
				break
			case '^':
				e.preventDefault()
				actions.ButtonClick({target:{value:'HEX'}})
				break
			default:
				// no ctrl command detected
				break
		}
	} else {
		// no ctrl command detected
	}
}

export const randFromArr = xs =>
	(xs[Math.floor(Math.random() * xs.length)])

export const sum = (x, y) =>
	x + y
