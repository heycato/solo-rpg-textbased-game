import {randFromArr} from './index.js'

export const generateRandomEvent = model =>
	([
		[randFromArr(model.ACTION_FOCUS), randFromArr(model.SUIT_DOMAIN)],
		[randFromArr(model.TOPIC_FOCUS), randFromArr(model.SUIT_DOMAIN)]
	])

export const displayRandomEvent = ([actionFocus, topicFocus]) =>
`- Action: ${actionFocus[0]}, ${actionFocus[1]};
- Topic: ${topicFocus[0]}, ${topicFocus[1]}
`

export const generatePacingMove = model => {
	const move = randFromArr(model.PACING_MOVES)
	return move === 'RANDOM_EVENT' ?
		displayRandomEvent(generateRandomEvent(model)) :
		move
}

export const generateFailureMove = model =>
	randFromArr(model.FAILURE_MOVES)

const displayMove = move =>
	`Move: ${move}`

export const generateScene = model => {
	const complication = randFromArr(model.SCENE_COMPLICATION)
	const alteration = randFromArr(model.ALTERED_SCENE)
	switch(alteration) {
		case 'SCENE_COMPLICATION':
			return ([complication, randFromArr(model.SCENE_COMPLICATION)])
		case 'PACING_MOVE':
			return ([complication, generatePacingMove(model)])
		case 'RANDOM_EVENT':
			return ([complication, displayRandomEvent(generateRandomEvent(model))])
		default:
			return ([complication, alteration])
	}
}

export const displayScene = ([complication, alteration]) =>
`Scene
	Complication: ${complication}
	Alteration: ${alteration}
`

export const generatePlotHook = model =>
	([
		randFromArr(model.OBJECTIVE),
		randFromArr(model.ADVERSARIES),
		randFromArr(model.REWARDS),
	])

export const askYesNo = (model, odds) =>
	`${randFromArr(model.ORACLE_YESNO[odds])} ${randFromArr(model.ORACLE_YESNO_MOD)}`

export const askHowMany = model =>
	randFromArr(model.ORACLE_HOW)

export const askActionFocus = model =>
	([
		randFromArr(model.ACTION_FOCUS),
		randFromArr(model.SUIT_DOMAIN)
	])

export const askDetailFocus = model =>
	([
		randFromArr(model.DETAIL_FOCUS),
		randFromArr(model.SUIT_DOMAIN)
	])

export const askTopicFocus = model =>
	([
		randFromArr(model.TOPIC_FOCUS),
		randFromArr(model.SUIT_DOMAIN)
	])

export const askGeneralQuestion = model =>
	([
		askActionFocus(model),
		askDetailFocus(model),
		askHowMany(model)
	])

export const generateNPC = model =>
	([
		randFromArr(model.IDENTITY),
		randFromArr(model.GOAL),
		randFromArr(model.FEATURES),
		askHowMany(model),
		askTopicFocus(model)
	])

export const generateDungeon = model =>
	([
		randFromArr(model.LOCATION),
		randFromArr(model.ENCOUNTER),
		randFromArr(model.OBJECT),
		randFromArr(model.TOTAL_EXITS),
	])

export const generateHex = model =>
	([
		randFromArr(model.TERRAIN),
		randFromArr(model.CONTENTS),
		randFromArr(model.FEATURES),
		randFromArr(model.EVENT),
	])

export const generateFeatures = model =>
	randFromArr(model.FEATURES)

export const generateTerrain = model =>
	randFromArr(model.TERRAIN)

export const generateFateRoll = model =>
	([
		randFromArr(model.FATE_DICE),
		randFromArr(model.FATE_DICE),
		randFromArr(model.FATE_DICE),
		randFromArr(model.FATE_DICE),
	])
