# Solo RPG Text-based Writing Game

The game is based on these [rules](https://inflatablestudios.itch.io/one-page-solo-engine)

Serve the public directory with the server of your choice.
Once the game has been loaded - it is usable offline.

Game storage uses the browser's `localStorage`.

![Screenshot](./screenshot.png)

