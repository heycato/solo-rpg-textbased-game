import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import css from 'rollup-plugin-css-only'

export default {
	input: 'src/main.js',
	output: {
		file: 'public/bundle.js',
		format: 'iife'
	},
	plugins: [
		resolve(),
		commonjs(),
		css({ output: 'bundle.css' })
	]
}
