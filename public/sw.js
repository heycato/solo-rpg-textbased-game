const CACHE_NAME = 'SoloRPGWriter'
const ASSETS = [
	'/',
	'/bundle.css',
	'/bundle.js',
	'/data.json',
]

self.addEventListener('fetch', e => {
	if(e.request.url === 'http://192.168.0.160:8080' || e.request.url === 'http://localhost:8080') {
		e.respondWith(
			fetch(e.request)
				.catch(err =>
					self.cache.open(CACHE_NAME)
						.then(cache => cache.match('/'))
				)
		)
	} else {
		e.respondWith(
			fetch(e.request)
				.catch(err =>
					caches.match(e.request).then(response => response)
				)
		)
	}
})

self.addEventListener('install', e => {
	console.log('installing...')
	e.waitUntil(
		caches
			.open(CACHE_NAME)
			.then(cache => cache.addAll(ASSETS))
			.catch(err => console.log(err))
	)
})
