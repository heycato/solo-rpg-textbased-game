(function () {
  'use strict';

  function h(name, attributes) {
    var rest = [];
    var children = [];
    var length = arguments.length;

    while (length-- > 2) rest.push(arguments[length]);

    while (rest.length) {
      var node = rest.pop();
      if (node && node.pop) {
        for (length = node.length; length--; ) {
          rest.push(node[length]);
        }
      } else if (node != null && node !== true && node !== false) {
        children.push(node);
      }
    }

    return typeof name === "function"
      ? name(attributes || {}, children)
      : {
          nodeName: name,
          attributes: attributes || {},
          children: children,
          key: attributes && attributes.key
        }
  }

  function app(state, actions, view, container) {
    var map = [].map;
    var rootElement = (container && container.children[0]) || null;
    var oldNode = rootElement && recycleElement(rootElement);
    var lifecycle = [];
    var skipRender;
    var isRecycling = true;
    var globalState = clone(state);
    var wiredActions = wireStateToActions([], globalState, clone(actions));

    scheduleRender();

    return wiredActions

    function recycleElement(element) {
      return {
        nodeName: element.nodeName.toLowerCase(),
        attributes: {},
        children: map.call(element.childNodes, function(element) {
          return element.nodeType === 3 // Node.TEXT_NODE
            ? element.nodeValue
            : recycleElement(element)
        })
      }
    }

    function resolveNode(node) {
      return typeof node === "function"
        ? resolveNode(node(globalState, wiredActions))
        : node != null
          ? node
          : ""
    }

    function render() {
      skipRender = !skipRender;

      var node = resolveNode(view);

      if (container && !skipRender) {
        rootElement = patch(container, rootElement, oldNode, (oldNode = node));
      }

      isRecycling = false;

      while (lifecycle.length) lifecycle.pop()();
    }

    function scheduleRender() {
      if (!skipRender) {
        skipRender = true;
        setTimeout(render);
      }
    }

    function clone(target, source) {
      var out = {};

      for (var i in target) out[i] = target[i];
      for (var i in source) out[i] = source[i];

      return out
    }

    function setPartialState(path, value, source) {
      var target = {};
      if (path.length) {
        target[path[0]] =
          path.length > 1
            ? setPartialState(path.slice(1), value, source[path[0]])
            : value;
        return clone(source, target)
      }
      return value
    }

    function getPartialState(path, source) {
      var i = 0;
      while (i < path.length) {
        source = source[path[i++]];
      }
      return source
    }

    function wireStateToActions(path, state, actions) {
      for (var key in actions) {
        typeof actions[key] === "function"
          ? (function(key, action) {
              actions[key] = function(data) {
                var result = action(data);

                if (typeof result === "function") {
                  result = result(getPartialState(path, globalState), actions);
                }

                if (
                  result &&
                  result !== (state = getPartialState(path, globalState)) &&
                  !result.then // !isPromise
                ) {
                  scheduleRender(
                    (globalState = setPartialState(
                      path,
                      clone(state, result),
                      globalState
                    ))
                  );
                }

                return result
              };
            })(key, actions[key])
          : wireStateToActions(
              path.concat(key),
              (state[key] = clone(state[key])),
              (actions[key] = clone(actions[key]))
            );
      }

      return actions
    }

    function getKey(node) {
      return node ? node.key : null
    }

    function eventListener(event) {
      return event.currentTarget.events[event.type](event)
    }

    function updateAttribute(element, name, value, oldValue, isSvg) {
      if (name === "key") ; else if (name === "style") {
        if (typeof value === "string") {
          element.style.cssText = value;
        } else {
          if (typeof oldValue === "string") oldValue = element.style.cssText = "";
          for (var i in clone(oldValue, value)) {
            var style = value == null || value[i] == null ? "" : value[i];
            if (i[0] === "-") {
              element.style.setProperty(i, style);
            } else {
              element.style[i] = style;
            }
          }
        }
      } else {
        if (name[0] === "o" && name[1] === "n") {
          name = name.slice(2);

          if (element.events) {
            if (!oldValue) oldValue = element.events[name];
          } else {
            element.events = {};
          }

          element.events[name] = value;

          if (value) {
            if (!oldValue) {
              element.addEventListener(name, eventListener);
            }
          } else {
            element.removeEventListener(name, eventListener);
          }
        } else if (
          name in element &&
          name !== "list" &&
          name !== "type" &&
          name !== "draggable" &&
          name !== "spellcheck" &&
          name !== "translate" &&
          !isSvg
        ) {
          element[name] = value == null ? "" : value;
        } else if (value != null && value !== false) {
          element.setAttribute(name, value);
        }

        if (value == null || value === false) {
          element.removeAttribute(name);
        }
      }
    }

    function createElement(node, isSvg) {
      var element =
        typeof node === "string" || typeof node === "number"
          ? document.createTextNode(node)
          : (isSvg = isSvg || node.nodeName === "svg")
            ? document.createElementNS(
                "http://www.w3.org/2000/svg",
                node.nodeName
              )
            : document.createElement(node.nodeName);

      var attributes = node.attributes;
      if (attributes) {
        if (attributes.oncreate) {
          lifecycle.push(function() {
            attributes.oncreate(element);
          });
        }

        for (var i = 0; i < node.children.length; i++) {
          element.appendChild(
            createElement(
              (node.children[i] = resolveNode(node.children[i])),
              isSvg
            )
          );
        }

        for (var name in attributes) {
          updateAttribute(element, name, attributes[name], null, isSvg);
        }
      }

      return element
    }

    function updateElement(element, oldAttributes, attributes, isSvg) {
      for (var name in clone(oldAttributes, attributes)) {
        if (
          attributes[name] !==
          (name === "value" || name === "checked"
            ? element[name]
            : oldAttributes[name])
        ) {
          updateAttribute(
            element,
            name,
            attributes[name],
            oldAttributes[name],
            isSvg
          );
        }
      }

      var cb = isRecycling ? attributes.oncreate : attributes.onupdate;
      if (cb) {
        lifecycle.push(function() {
          cb(element, oldAttributes);
        });
      }
    }

    function removeChildren(element, node) {
      var attributes = node.attributes;
      if (attributes) {
        for (var i = 0; i < node.children.length; i++) {
          removeChildren(element.childNodes[i], node.children[i]);
        }

        if (attributes.ondestroy) {
          attributes.ondestroy(element);
        }
      }
      return element
    }

    function removeElement(parent, element, node) {
      function done() {
        parent.removeChild(removeChildren(element, node));
      }

      var cb = node.attributes && node.attributes.onremove;
      if (cb) {
        cb(element, done);
      } else {
        done();
      }
    }

    function patch(parent, element, oldNode, node, isSvg) {
      if (node === oldNode) ; else if (oldNode == null || oldNode.nodeName !== node.nodeName) {
        var newElement = createElement(node, isSvg);
        parent.insertBefore(newElement, element);

        if (oldNode != null) {
          removeElement(parent, element, oldNode);
        }

        element = newElement;
      } else if (oldNode.nodeName == null) {
        element.nodeValue = node;
      } else {
        updateElement(
          element,
          oldNode.attributes,
          node.attributes,
          (isSvg = isSvg || node.nodeName === "svg")
        );

        var oldKeyed = {};
        var newKeyed = {};
        var oldElements = [];
        var oldChildren = oldNode.children;
        var children = node.children;

        for (var i = 0; i < oldChildren.length; i++) {
          oldElements[i] = element.childNodes[i];

          var oldKey = getKey(oldChildren[i]);
          if (oldKey != null) {
            oldKeyed[oldKey] = [oldElements[i], oldChildren[i]];
          }
        }

        var i = 0;
        var k = 0;

        while (k < children.length) {
          var oldKey = getKey(oldChildren[i]);
          var newKey = getKey((children[k] = resolveNode(children[k])));

          if (newKeyed[oldKey]) {
            i++;
            continue
          }

          if (newKey != null && newKey === getKey(oldChildren[i + 1])) {
            if (oldKey == null) {
              removeElement(element, oldElements[i], oldChildren[i]);
            }
            i++;
            continue
          }

          if (newKey == null || isRecycling) {
            if (oldKey == null) {
              patch(element, oldElements[i], oldChildren[i], children[k], isSvg);
              k++;
            }
            i++;
          } else {
            var keyedNode = oldKeyed[newKey] || [];

            if (oldKey === newKey) {
              patch(element, keyedNode[0], keyedNode[1], children[k], isSvg);
              i++;
            } else if (keyedNode[0]) {
              patch(
                element,
                element.insertBefore(keyedNode[0], oldElements[i]),
                keyedNode[1],
                children[k],
                isSvg
              );
            } else {
              patch(element, oldElements[i], null, children[k], isSvg);
            }

            newKeyed[newKey] = children[k];
            k++;
          }
        }

        while (i < oldChildren.length) {
          if (getKey(oldChildren[i]) == null) {
            removeElement(element, oldElements[i], oldChildren[i]);
          }
          i++;
        }

        for (var i in oldKeyed) {
          if (!newKeyed[i]) {
            removeElement(element, oldKeyed[i][0], oldKeyed[i][1]);
          }
        }
      }
      return element
    }
  }

  function defaultLog(prevState, action, nextState) {
    console.group("%c action", "color: gray; font-weight: lighter;", action.name);
    console.log("%c prev state", "color: #9E9E9E; font-weight: bold;", prevState);
    console.log("%c data", "color: #03A9F4; font-weight: bold;", action.data);
    console.log("%c next state", "color: #4CAF50; font-weight: bold;", nextState);
    console.groupEnd();
  }

  var isFn = function(value) {
    return typeof value === "function"
  };

  function makeLoggerApp(log, nextApp) {
    return function(initialState, actionsTemplate, view, container) {
      function enhanceActions(actions, prefix) {
        var namespace = prefix ? prefix + "." : "";
        return Object.keys(actions || {}).reduce(function(otherActions, name) {
          var namedspacedName = namespace + name;
          var action = actions[name];
          otherActions[name] =
            typeof action === "function"
              ? function(data) {
                  return function(state, actions) {
                    var result = action(data);
                    result =
                      typeof result === "function"
                        ? result(state, actions)
                        : result;
                    log(state, { name: namedspacedName, data: data }, result);
                    return result
                  }
                }
              : enhanceActions(action, namedspacedName);
          return otherActions
        }, {})
      }

      var enhancedActions = enhanceActions(actionsTemplate);

      var appActions = nextApp(initialState, enhancedActions, view, container);
      return appActions
    }
  }

  function withLogger(optionsOrApp) {
    if (isFn(optionsOrApp)) {
      return makeLoggerApp(defaultLog, optionsOrApp)
    } else {
      var log = isFn(optionsOrApp.log) ? optionsOrApp.log : defaultLog;
      return function(nextApp) {
        return makeLoggerApp(log, nextApp)
      }
    }
  }

  const createContentItem = (label, value) =>
  	h('div', {class:'card-item'},
  		h('div', {class:'card-item-label'}, `${label}:`),
  		h('div', {class:'card-item-value'}, value)
  	);

  const createEventContentItem = event =>
  	(['Event']).concat(Object.entries(event)
  		.map(([label, value]) => createContentItem(label, value.join(', '))));

  var Card = actions => (card, id) =>
  	h('div', {
  		key:`card-id-${id}`,
  		class:`card ${card.type}`
  	},
  		h('div', {
  			class:'card-header'
  		},
  			h('div', {},
  				h('button', {
  					class:'card-edit-button',
  					onclick: () => actions.UpdateOverlayContent({content:card, isEdit:true})
  				}, 'edit'),
  				h('button', {
  					class:'card-remove-button',
  					onclick: () => actions.RemoveCard(id)
  				}, 'remove'),
  			)
  		),
  		(card.content.scene || card.content.question ?
  			h('div', {
  				class:'card-scene'
  			}, card.content.scene || card.content.question) : ''),
  		h('div', {
  			class:'card-content'
  		},
  			...Object.entries(card.content)
  			.filter(([label]) => !/description|scene|question/.test(label))
  				.map(([label, value]) =>
  					label === 'event' ?
  						createEventContentItem(value) :
  						createContentItem(label, value)
  				)
  		),
  		h('div', {
  			class:'card-description'
  		}, card.content.description)
  	);

  var CardDisplay = (state, actions) =>
  	h('div', {
  		class:`card-display ${state.overlay ? 'blur' : ''}`,
  	},
  		h('div', { class:'card-display-title', key:'card-display-title'}, state.game ? state.game : 'Untitled Game'),
  		h('div', { class:'card-scroller'},
  			...state.cards.map(Card(actions))
  		)
  	);

  var ControlButton = (label, action, command) =>
  	h('button', {
  		title:`${label} (ctrl + ${command})`,
  		class:'control-button',
  		value:action
  	},
  		label
  	);

  var ButtonGroup = (title, ...children) =>
  	h('div', {
  		class:`button-group ${title.replace(/\s/g, '').toLowerCase()}`
  	},
  		h('div', { class: 'title' }, title),
  		...children
  	);

  var ControlPanel = (state, actions) =>
  	h('div', {
  		class:`control-panel ${state.overlay ? 'blur' : ''}`,
  		onclick: actions.ButtonClick
  	},
  		ButtonGroup(
  			'Actions',
  			ControlButton('Add a Story', 'ADD_STORY', '`'),
  			ControlButton('Add a Player Action', 'PLAYER_ACTION', '1'),
  			ControlButton('Set up the Scene', 'SET_THE_SCENE', '2'),
  			ControlButton('What next?', 'PACING_MOVE', '3'),
  			ControlButton('Add a failure', 'FAILURE_MOVE', '4'),
  		),
  		ButtonGroup(
  			'Ask The GM',
  			ControlButton('Ask Yes or No?', 'ORACLE_YESNO', '6'),
  			ControlButton('Ask quantity or quality', 'ORACLE_HOW', '7'),
  			ControlButton('What does it do?', 'ACTION_FOCUS', '8'),
  			ControlButton('What kind of thing?', 'DETAIL_FOCUS', '9'),
  			ControlButton('What is this about?', 'TOPIC_FOCUS', '!'),
  			ControlButton('General Questions', 'GENERAL', '@'),
  		),
  		ButtonGroup(
  			'Generators',
  			ControlButton('Random Event',  'RANDOM_EVENT', '5'),
  			ControlButton('Plot Hook', 'PLOT_HOOK', '#'),
  			ControlButton('NPC', 'NPC', '$'),
  			ControlButton('Dungeon or Structure', 'DUNGEON', '%'),
  			ControlButton('Environment', 'HEX', '^'),
  			ControlButton('Environment Features', 'FEATURES', '&'),
  			ControlButton('Terrain', 'TERRAIN', '*'),
  		),
  		ButtonGroup(
  			'Utilities',
  			ControlButton('New Game', 'NEW_GAME', 'n'),
  			ControlButton('Save', 'SAVE', 's'),
  			ControlButton('Load Game', 'LOAD_GAME', 'l'),
  			h('button', {
  				title:'Download (ctrl + d)',
  				class:'control-button',
  				onclick:actions.ExportFile
  			}, 'Download Current Game'),
  			// ControlButton('Dice Roller', 'DICE_ROLLER', ' '),
  		)

  	);

  var Overlay = (state, actions, content) =>
  	h('div', {
  		class:content ? 'overlay' : 'overlay hide',
  		onclick: actions.CloseOverlay
  	},
  		h('div', {
  			class:'overlay-content'
  		}, content)
  	);

  var AddStoryOverlay = (state, actions) =>
  	h('div', {
  		class:'add-story-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Add A Story'),
  		h('div', { class:'add-story-input'},
  			h('div', { class:'overlay-label'}, 'Describe what happened:'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							description:e.target.innerText
  						}
  					},
  					isEdit:state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.description : '')
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add story' }`),
  		)
  	);

  var PlayerActionOverlay = (state, actions) =>
  	h('div', {
  		class:'player-action-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Add a Player Action'),
  		h('div', { class:'player-action-input'},
  			h('div', { class:'overlay-label'}, 'What did they do/attempt? (skill check, or action)'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							action:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.action : '')
  		),
  		h('div', {
  			class:'dice-side-input'
  		},
  			h('div', {
  				class:'fate-dice-container',
  				onclick:actions.RollFate
  			},
  				...state.overlay.content.roll
  					.map(([sym]) =>
  						h('div', {class:'fate-die-frame'},
  							h('div', {class:'fate-die-face'},
  								h('div', {class:'fate-die-symbol'}, sym)
  							)
  						)
  					),
  				h('div', {
  					class: `fate-roll-result ${state.overlay.content.result >= 0 ? 'pos' : 'neg'}`
  				}, state.overlay.content.result > 0 ? `+${state.overlay.content.result}` : state.overlay.content.result),
  			),
  		),
  		h('div', { class:'player-action-input'},
  			h('div', { class:'overlay-label'}, 'What was the result? (skill roll, pass/fail)'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							outcome:e.target.innerText
  						}
  					},
  					isEdit:state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.outcome : '')
  		),
  		h('div', { class:'player-action-input'},
  			h('div', { class:'overlay-label'}, 'Describe what happened: (tell the story)'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							description:e.target.innerText
  						}
  					},
  					isEdit:state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.description : '')
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add action' }`),
  		)
  	);

  const buildGeneratedDisplay$f = generatedContent =>
  	h('div', {
  		class:'set-the-scene-input'
  	},
  		h('div', {}, `Complication: ${generatedContent.complication}`),
  		h('div', {}, `Alteration: ${generatedContent.alteration}`),
  	);

  var SetTheSceneOverlay = (state, actions) =>
  	h('div', {
  		class:'set-the-scene-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Set Up The Scene'),
  		h('div', { class:'set-the-scene-input'},
  			h('div', { class:'overlay-label'}, 'How does the scene start? What is your character doing and hope to accomplish?'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							scene:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.scene : '')
  		),
  		h('div', {
  			class:`set-the-scene-generator`
  		},
  			h('div', {
  				class:'set-the-scene-input'
  			},
  				state.overlay?.content?.complication ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$f(state.overlay.content)) :
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateScene
  					}, 'generate complications')
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.complication ? 'open' : 'close'}`
  			},
  				h('div', { class:'set-the-scene-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.result : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add scene' }`),
  		)
  	);

  const buildGeneratedDisplay$e = generatedContent =>
  	h('div', {
  		class:'pacing-move-input'
  	},
  		h('div', {}, `Move: ${generatedContent.move}`),
  	);

  var PacingMoveOverlay = (state, actions) =>
  	h('div', {
  		class:'pacing-move-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'What happens next?'),
  		h('div', {
  			class:`pacing-move-generator`
  		},
  			h('div', {
  				class:'pacing-move-input'
  			},
  				state.overlay?.content?.move ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$e(state.overlay.content)) :
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GeneratePacingMove
  					}, 'generate prompt')
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.move ? 'open' : 'close'}`
  			},
  				h('div', { class:'pacing-move-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.result : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add move' }`),
  		)
  	);

  const buildGeneratedDisplay$d = generatedContent =>
  	h('div', {
  		class:'failure-move-input'
  	},
  		h('div', {}, `Move: ${generatedContent.move}`),
  	);

  var FailureMoveOverlay = (state, actions) =>
  	h('div', {
  		class:'failure-move-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Add A Failure Consequence'),
  		h('div', {
  			class:`failure-move-generator`
  		},
  			h('div', {
  				class:'failure-move-input'
  			},
  				state.overlay?.content?.move ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$d(state.overlay.content)) :
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateFailureMove
  					}, 'generate consequence')
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.move ? 'open' : 'close'}`
  			},
  				h('div', { class:'failure-move-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.result : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add move' }`),
  		)
  	);

  const buildGeneratedDisplay$c = generatedContent =>
  	h('div', {
  		class:'random-event-input'
  	},
  		h('div', {}, `Action: ${generatedContent.event.action[0]}, ${generatedContent.event.action[1]}`),
  		h('div', {}, `Topic: ${generatedContent.event.topic[0]}, ${generatedContent.event.topic[1]}`),
  	);

  var RandomEventOverlay = (state, actions) =>
  	h('div', {
  		class:'random-event-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Generate a Random Event'),
  		h('div', {
  			class:`random-event-generator`
  		},
  			h('div', {
  				class:'random-event-input'
  			},
  				state.overlay?.content?.event ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$c(state.overlay.content)) :
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateRandomEvent
  					}, 'generate event')
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.event ? 'open' : 'close'}`
  			},
  				h('div', { class:'random-event-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.result : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add event' }`),
  		)
  	);

  const buildGeneratedDisplay$b = generatedContent =>
  	h('div', {
  		class:'plot-hook-input'
  	},
  		h('div', {}, `Objective: ${generatedContent.objective}`),
  		h('div', {}, `Adversaries: ${generatedContent.adversaries}`),
  		h('div', {}, `Rewards: ${generatedContent.rewards}`),
  	);

  var PlotHookOverlay = (state, actions) =>
  	h('div', {
  		class:'plot-hook-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Generate a Plot Hook'),
  		h('div', {
  			class:`plot-hook-generator`
  		},
  			h('div', {
  				class:'plot-hook-input'
  			},
  				state.overlay?.content?.objective ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$b(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GeneratePlotHook,
  					}, 'generate plot hook')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.objective ? 'open' : 'close'}`
  			},
  				h('div', { class:'plot-hook-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add plot hook' }`),
  		)
  	);

  const buildGeneratedDisplay$a = generatedContent =>
  	h('div', {
  		class:'npc-input'
  	},
  		h('div', {}, `Identity: ${generatedContent.identity}`),
  		h('div', {}, `Goal: ${generatedContent.goal}`),
  		h('div', {}, `Features: ${generatedContent.features}`),
  		h('div', {}, `Attitude to PCs: ${generatedContent.attitude}`),
  		h('div', {}, `Converstation: ${generatedContent.conversation}`),
  	);

  var NPCOverlay = (state, actions) =>
  	h('div', {
  		class:'npc-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Generate an NPC'),
  		h('div', {
  			class:`npc-generator`
  		},
  			h('div', {
  				class:'npc-input'
  			},
  				state.overlay?.content?.identity ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$a(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateNPC,
  					}, 'generate npc')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.identity ? 'open' : 'close'}`
  			},
  				h('div', { class:'npc-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add npc' }`),
  		)
  	);

  const buildRadioButton = (selected, action) => value =>
  	h('div', {
  		class:'radio-button',
  		onclick: () => action(value)
  	},
  		h('div', {
  			class:`radio-indicator ${selected === value ? 'selected' : ''}`
  		}),
  		h('div', {
  			class:'radio-label'
  		}, value),
  	);

  var RadioGroup = (selected, options, action) =>
  	h('div', {
  		class:'radio-group'
  	},
  		...options.map(buildRadioButton(selected, action))
  	);

  const buildGeneratedDisplay$9 = generatedContent =>
  	h('div', {
  		class:'dungeon-input'
  	},
  		...Object.entries(generatedContent)
  			.filter(([key, value]) => !!value)
  			.map(([key, value]) =>
  				h('div', {}, `${key.charAt(0).toUpperCase() + key.slice(1)}: ${value}`))
  	);

  var DungeonOverlay = (state, actions) =>
  	h('div', {
  		class:'dungeon-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Generate a Dungeon or Structure'),
  		h('div', {
  			class:`dungeon-generator`
  		},
  			h('div', {
  				class:'dungeon-input'
  			},
  				state.overlay?.content?.location ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$9(state.overlay.content)) :
  				h('div', {},
  					RadioGroup(state.overlay.content.type, ['Room', 'Entrance'], actions.SetDungeonType),
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateDungeon,
  					}, 'generate dungeon')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.location ? 'open' : 'close'}`
  			},
  				h('div', { class:'dungeon-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add dungeon' }`),
  		)
  	);

  const buildGeneratedDisplay$8 = generatedContent =>
  	h('div', {
  		class:'hex-input'
  	},
  		...Object.entries(generatedContent)
  			.map(([key, value]) =>
  				h('div', {}, `${key.charAt(0).toUpperCase() + key.slice(1)}: ${value}`))
  	);

  var HexOverlay = (state, actions) =>
  	h('div', {
  		class:'hex-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Generate an environment'),
  		h('div', {
  			class:`hex-generator`
  		},
  			h('div', {
  				class:'hex-input'
  			},
  				state.overlay?.content?.terrain ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$8(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateHex,
  					}, 'generate environment')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.terrain ? 'open' : 'close'}`
  			},
  				h('div', { class:'hex-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add hex' }`),
  		)
  	);

  const buildGeneratedDisplay$7 = generatedContent =>
  	h('div', {
  		class:'features-input'
  	},
  		h('div', {}, `Features: ${generatedContent.features}`),
  	);

  var FeaturesOverlay = (state, actions) =>
  	h('div', {
  		class:'features-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Generate environment Features'),
  		h('div', {
  			class:`features-generator`
  		},
  			h('div', {
  				class:'features-input'
  			},
  				state.overlay?.content?.features ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$7(state.overlay.content)) :
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateFeatures
  					}, 'generate features')
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.features ? 'open' : 'close'}`
  			},
  				h('div', { class:'features-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add features' }`),
  		)
  	);

  const buildGeneratedDisplay$6 = generatedContent =>
  	h('div', {
  		class:'terrain-input'
  	},
  		h('div', {}, `Terrain: ${generatedContent.terrain}`),
  	);

  var TerrainOverlay = (state, actions) =>
  	h('div', {
  		class:'terrain-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Generate Terrain'),
  		h('div', {
  			class:`terrain-generator`
  		},
  			h('div', {
  				class:'terrain-input'
  			},
  				state.overlay?.content?.terrain ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$6(state.overlay.content)) :
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.GenerateTerrain
  					}, 'generate terrain')
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.terrain ? 'open' : 'close'}`
  			},
  				h('div', { class:'terrain-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add terrain' }`),
  		)
  	);

  const clamp = (min, max) => n =>
  	n < min ? min :
  		n > max ? max :
  		n;

  const createKeyCommandsExec = (actions, history) => e => {
  	if(e.defaultPrevented) return
  	if(e.ctrlKey) {
  		switch(e.key) {
  			case 'z': //undo
  				let prevState = history.undo();
  				actions.SetState(prevState);
  				e.preventDefault();
  				break
  			case 'r': //redo
  				let nextState = history.redo();
  				actions.SetState(nextState);
  				e.preventDefault();
  				break
  			case 'n':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'NEW_GAME'}});
  				break
  			case 's':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'SAVE'}});
  				break
  			case 'd':
  				e.preventDefault();
  				actions.DownloadFile();
  				break
  			case 'l':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'LOAD_GAME'}});
  				break
  			case ' ':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'DICE_ROLLER'}});
  				break
  			case '`':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'ADD_STORY'}});
  				break
  			case '1':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'PLAYER_ACTION'}});
  				break
  			case '2':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'SET_THE_SCENE'}});
  				break
  			case '3':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'PLACING_MOVE'}});
  				break
  			case '4':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'FAILURE_MOVE'}});
  				break
  			case '5':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'RANDOM_EVENT'}});
  				break
  			case '6':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'ORACLE_YESNO'}});
  				break
  			case '7':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'ORACLE_HOW'}});
  				break
  			case '8':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'ACTION_FOCUS'}});
  				break
  			case '9':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'DETAIL_FOCUS'}});
  				break
  			case '!':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'TOPIC_FOCUS'}});
  				break
  			case '@':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'GENERAL'}});
  				break
  			case '#':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'PLOT_HOOK'}});
  				break
  			case '$':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'NPC'}});
  				break
  			case '%':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'DUNGEON'}});
  				break
  			case '^':
  				e.preventDefault();
  				actions.ButtonClick({target:{value:'HEX'}});
  				break
  		}
  	}
  };

  const randFromArr = xs =>
  	(xs[Math.floor(Math.random() * xs.length)]);

  const sum = (x, y) =>
  	x + y;

  const buildFateDiceRoller = (state, actions) =>
  	h('div', {
  		class:'dice-side-input'
  	},
  		h('div', {
  			class:'fate-dice-container'
  		},
  			...state.overlay.content.roll
  				.map(([sym]) =>
  					h('div', {class:'fate-die-frame'},
  						h('div', {class:'fate-die-face'},
  							h('div', {class:'fate-die-symbol'}, sym)
  						)
  					)
  				),
  			h('div', {
  				class: 'fate-roll-result'
  			}, state.overlay.content.result > 0 ? `+${state.overlay.content.result}` : state.overlay.content.result),
  		),
  	);

  var DiceRollerOverlay = (state, actions) =>
  	h('div', {
  		class:'dice-roller-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Fate Dice Roller'),
  		// RadioGroup(state.overlay.content.type, ['Multi-sided', 'Fate'], actions.SelectDiceType),
  		// h('div', {
  		// 	class:'dice-roller-container'
  		// },
  		// 	(state.overlay.content.type === 'Multi-sided' ?  buildMultiSidedDiceRoller(state, actions) :
  		// 		state.overlay.content.type === 'Fate' ? buildFateDiceRoller(state, actions) : '')
  		// ),
  		buildFateDiceRoller(state),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'close'),
  			h('button', {
  				class:'apply-button',
  				onclick:actions.RollFate
  			}, 'roll'),
  		)
  	);

  const buildGeneratedDisplay$5 = generatedContent =>
  	h('div', {
  		class:'oracle-yesno-input'
  	},
  		h('div', {}, `Answer: ${generatedContent.answer}`),
  	);

  var OracleYesNoOverlay = (state, actions) =>
  	h('div', {
  		class:'oracle-yesno-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Ask a Yes or No Question'),
  		h('div', { class:'oracle-yesno-input'},
  			h('div', { class:'overlay-label'}, 'Ask your question:'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							question:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.question : '')
  		),
  		h('div', {
  			class:`oracle-yesno-generator`
  		},
  			h('div', {
  				class:'oracle-yesno-input'
  			},
  				state.overlay?.content?.answer ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$5(state.overlay.content)) :
  				h('div', {},
  					RadioGroup(state.overlay.content.odds, Object.keys(state.model.ORACLE_YESNO), actions.SetOdds),
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.AskYesNo,
  						disabled: !state.overlay.content.odds
  					}, 'ask question')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.answer ? 'open' : 'close'}`
  			},
  				h('div', { class:'oracle-yesno-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
  		)
  	);

  const buildGeneratedDisplay$4 = generatedContent =>
  	h('div', {
  		class:'oracle-how-input'
  	},
  		h('div', {}, `Answer: ${generatedContent.answer}`),
  	);

  var OracleHowOverlay = (state, actions) =>
  	h('div', {
  		class:'oracle-how-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Ask about quantity or quality'),
  		h('div', { class:'oracle-how-input'},
  			h('div', { class:'overlay-label'}, 'Ask your question:'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							question:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.question : '')
  		),
  		h('div', {
  			class:`oracle-how-generator`
  		},
  			h('div', {
  				class:'oracle-how-input'
  			},
  				state.overlay?.content?.answer ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$4(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.AskHowMany,
  					}, 'ask question')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.answer ? 'open' : 'close'}`
  			},
  				h('div', { class:'oracle-how-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
  		)
  	);

  const buildGeneratedDisplay$3 = generatedContent =>
  	h('div', {
  		class:'oracle-action-focus-input'
  	},
  		h('div', {}, `Answer: ${generatedContent.answer}`),
  	);

  var OracleActionFocusOverlay = (state, actions) =>
  	h('div', {
  		class:'oracle-action-focus-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Ask a "What does it do?" Question'),
  		h('div', { class:'oracle-action-focus-input'},
  			h('div', { class:'overlay-label'}, 'Ask your question:'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							question:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.question : '')
  		),
  		h('div', {
  			class:`oracle-action-focus-generator`
  		},
  			h('div', {
  				class:'oracle-action-focus-input'
  			},
  				state.overlay?.content?.answer ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$3(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.AskActionFocus,
  					}, 'ask question')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.answer ? 'open' : 'close'}`
  			},
  				h('div', { class:'oracle-action-focus-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
  		)
  	);

  const buildGeneratedDisplay$2 = generatedContent =>
  	h('div', {
  		class:'oracle-detail-focus-input'
  	},
  		h('div', {}, `Answer: ${generatedContent.answer}`),
  	);

  var OracleDetailFocusOverlay = (state, actions) =>
  	h('div', {
  		class:'oracle-detail-focus-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Ask a "What kind of thing is it?" Question'),
  		h('div', { class:'oracle-detail-focus-input'},
  			h('div', { class:'overlay-label'}, 'Ask your question:'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							question:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.question : '')
  		),
  		h('div', {
  			class:`oracle-detail-focus-generator`
  		},
  			h('div', {
  				class:'oracle-detail-focus-input'
  			},
  				state.overlay?.content?.answer ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$2(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.AskDetailFocus,
  					}, 'ask question')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.answer ? 'open' : 'close'}`
  			},
  				h('div', { class:'oracle-detail-focus-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
  		)
  	);

  const buildGeneratedDisplay$1 = generatedContent =>
  	h('div', {
  		class:'oracle-topic-focus-input'
  	},
  		h('div', {}, `Answer: ${generatedContent.answer}`),
  	);

  var OracleTopicFocusOverlay = (state, actions) =>
  	h('div', {
  		class:'oracle-topic-focus-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Ask a "What is this about?" Question'),
  		h('div', { class:'oracle-topic-focus-input'},
  			h('div', { class:'overlay-label'}, 'Ask your question:'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							question:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.question : '')
  		),
  		h('div', {
  			class:`oracle-topic-focus-generator`
  		},
  			h('div', {
  				class:'oracle-topic-focus-input'
  			},
  				state.overlay?.content?.answer ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay$1(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.AskTopicFocus,
  					}, 'ask question')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.answer ? 'open' : 'close'}`
  			},
  				h('div', { class:'oracle-topic-focus-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
  		)
  	);

  const buildGeneratedDisplay = generatedContent =>
  	h('div', {
  		class:'oracle-general-input'
  	},
  		h('div', {}, `What it does: ${generatedContent.operation}`),
  		h('div', {}, `How it looks: ${generatedContent.appearance}`),
  		h('div', {}, `Significance: ${generatedContent.significance}`),
  	);

  var OracleGeneralOverlay = (state, actions) =>
  	h('div', {
  		class:'oracle-general-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Ask a vague or general question'),
  		h('div', { class:'oracle-general-input'},
  			h('div', { class:'overlay-label'}, 'Ask your question:'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content:{
  							...state.overlay.content,
  							question:e.target.innerText
  						}
  					},
  					isEdit: state.isEdit
  				})
  			}, state.isEdit ? state.overlay?.content?.question : '')
  		),
  		h('div', {
  			class:`oracle-general-generator`
  		},
  			h('div', {
  				class:'oracle-general-input'
  			},
  				state.overlay?.content?.operation ?
  					h('div', {
  						class: 'overlay-generate-display'
  					}, buildGeneratedDisplay(state.overlay.content)) :
  				h('div', {},
  					h('button', {
  						class: 'overlay-generate-button',
  						onclick: actions.AskGeneralQuestion,
  					}, 'ask question')
  				)
  			),
  			h('div', {
  				class:`collapse-container ${state.overlay?.content?.operation ? 'open' : 'close'}`
  			},
  				h('div', { class:'oracle-general-input'},
  					h('div', { class:'overlay-label'}, 'What is your interpretation?'),
  					h('div', {
  						class:'text-area',
  						contenteditable:true,
  						onblur: e => actions.UpdateOverlayContent({
  							content: {
  								...state.overlay,
  								content:{
  									...state.overlay.content,
  									description:e.target.innerText
  								}
  							},
  							isEdit:state.isEdit
  						})
  					}, state.isEdit ? state.overlay?.content?.description : '')
  				)
  			)
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: () => state.isEdit ? actions.EditCard(state.overlay.id) : actions.AddCard(),
  			}, `${state.isEdit ? 'apply edits' : 'add answer' }`),
  		)
  	);

  var NewGameOverlay = (state, actions) =>
  	h('div', {
  		class:'new-game-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Create a New Game'),
  		h('div', { class:'new-game-input'},
  			h('div', { class:'overlay-label'}, 'Be sure to save the current game before continuing. Unsaved changes will be lost.'),
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: actions.NewGame
  			}, 'continue'),
  		)
  	);

  var SaveOverlay = (state, actions) =>
  	h('div', {
  		class:'save-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Save Game'),
  		h('div', { class:'save-input'},
  			h('div', { class:'overlay-label'}, 'Enter a name for your game'),
  			h('div', {
  				class:'text-area',
  				contenteditable:true,
  				onblur: e => actions.UpdateOverlayContent({
  					content: {
  						...state.overlay,
  						content: {
  							gameName:e.target.innerText
  						}
  					}
  				})
  			}, state.game ? state.game : '')
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'apply-button',
  				onclick: actions.SaveGame
  			}, 'save game'),
  		)
  	);

  var LoadGameOverlay = (state, actions) =>
  	h('div', {
  		class:'load-game-overlay',
  	},
  		h('div', { class:'overlay-title'}, 'Load A Saved Game'),
  		h('div', { class:'load-game-input'},
  			h('div', { class:'overlay-label'}, 'Choose which game you want to continue playing.'),
  			...JSON.parse(localStorage.games)
  				.map(game =>
  					h('button', {
  						class: `load-game-button ${state.overlay.content?.game?.name === game.name ? 'load-game-selected' : ''}`,
  						onclick: () => actions.SelectGame(game)
  					}, game.name)
  			)
  		),
  		h('div', { class:'load-game-input'},
  			h('div', { class:'overlay-label'}, 'Or load from file:'),
  			h('input', {
  				class:'textarea',
  				type:'file',
  				accept:'.json',
  				onchange: e => actions.UploadFile(e.target.files[0])
  			})
  		),
  		h('div', {
  			class:'overlay-button-bar'
  		},
  			h('button', {
  				class:'cancel-button',
  				onclick: actions.CloseOverlay
  			}, 'cancel'),
  			h('button', {
  				class:'delete-button',
  				disabled:!state.overlay.content.game,
  				onclick: actions.DeleteGame
  			}, 'delete game'),
  			h('button', {
  				class:'apply-button',
  				onclick: actions.LoadGame,
  				disabled:!state.overlay.content.game,
  			}, 'load game'),
  		)
  	);

  class History {
  	constructor(store=[]) {
  		this.currentIndex = 0;
  		this.store = store;
  	}
  	undo(n=1) {
  		this.currentIndex = clamp(0, this.store.length - 1)(this.currentIndex - n);
  		return this.store[this.currentIndex]
  	}
  	redo(n=1) {
  		this.currentIndex = clamp(0, this.store.length - 1)(this.currentIndex + n);
  		return this.store[this.currentIndex]
  	}
  	push(x) {
  		this.store.push(x);
  		this.currentIndex = this.store.length - 1;
  		return x
  	}
  	getCurrent() {
  		return this.store[this.currentIndex]
  	}
  }

  const generateRandomEvent = model =>
  	([
  		[randFromArr(model.ACTION_FOCUS), randFromArr(model.SUIT_DOMAIN)],
  		[randFromArr(model.TOPIC_FOCUS), randFromArr(model.SUIT_DOMAIN)]
  	]);

  const displayRandomEvent = ([actionFocus, topicFocus]) =>
`- Action: ${actionFocus[0]}, ${actionFocus[1]};
- Topic: ${topicFocus[0]}, ${topicFocus[1]}
`;

  const generatePacingMove = model => {
  	const move = randFromArr(model.PACING_MOVES);
  	return move === 'RANDOM_EVENT' ?
  		displayRandomEvent(generateRandomEvent(model)) :
  		move
  };

  const generateFailureMove = model =>
  	randFromArr(model.FAILURE_MOVES);

  const generateScene = model => {
  	const complication = randFromArr(model.SCENE_COMPLICATION);
  	const alteration = randFromArr(model.ALTERED_SCENE);
  	switch(alteration) {
  		case 'SCENE_COMPLICATION':
  			return ([complication, randFromArr(model.SCENE_COMPLICATION)])
  		case 'PACING_MOVE':
  			return ([complication, generatePacingMove(model)])
  		case 'RANDOM_EVENT':
  			return ([complication, displayRandomEvent(generateRandomEvent(model))])
  		default:
  			return ([complication, alteration])
  	}
  };

  const generatePlotHook = model =>
  	([
  		randFromArr(model.OBJECTIVE),
  		randFromArr(model.ADVERSARIES),
  		randFromArr(model.REWARDS),
  	]);

  const askYesNo = (model, odds) =>
  	`${randFromArr(model.ORACLE_YESNO[odds])} ${randFromArr(model.ORACLE_YESNO_MOD)}`;

  const askHowMany = model =>
  	randFromArr(model.ORACLE_HOW);

  const askActionFocus = model =>
  	([
  		randFromArr(model.ACTION_FOCUS),
  		randFromArr(model.SUIT_DOMAIN)
  	]);

  const askDetailFocus = model =>
  	([
  		randFromArr(model.DETAIL_FOCUS),
  		randFromArr(model.SUIT_DOMAIN)
  	]);

  const askTopicFocus = model =>
  	([
  		randFromArr(model.TOPIC_FOCUS),
  		randFromArr(model.SUIT_DOMAIN)
  	]);

  const askGeneralQuestion = model =>
  	([
  		askActionFocus(model),
  		askDetailFocus(model),
  		askHowMany(model)
  	]);

  const generateNPC = model =>
  	([
  		randFromArr(model.IDENTITY),
  		randFromArr(model.GOAL),
  		randFromArr(model.FEATURES),
  		askHowMany(model),
  		askTopicFocus(model)
  	]);

  const generateDungeon = model =>
  	([
  		randFromArr(model.LOCATION),
  		randFromArr(model.ENCOUNTER),
  		randFromArr(model.OBJECT),
  		randFromArr(model.TOTAL_EXITS),
  	]);

  const generateHex = model =>
  	([
  		randFromArr(model.TERRAIN),
  		randFromArr(model.CONTENTS),
  		randFromArr(model.FEATURES),
  		randFromArr(model.EVENT),
  	]);

  const generateFeatures = model =>
  	randFromArr(model.FEATURES);

  const generateTerrain = model =>
  	randFromArr(model.TERRAIN);

  const generateFateRoll = model =>
  	([
  		randFromArr(model.FATE_DICE),
  		randFromArr(model.FATE_DICE),
  		randFromArr(model.FATE_DICE),
  		randFromArr(model.FATE_DICE),
  	]);

  if('serviceWorker' in navigator) {
  	navigator.serviceWorker
  		.register('sw.js')
  		.then(() =>
  			console.log('registered service worker')
  		);
  }

  // hack to unregister service workers from safari
  // navigator.serviceWorker.getRegistrations().then(function(registrations) {
  //	for(let registration of registrations) {
  //	console.log('unregistering', registration)
  //	 registration.unregister()
  // } })

  if(!localStorage.games) localStorage.games = '[]';

  const history = new History();

  const historyApp = withLogger({
  	log(prevState, action, nextState) {
  		const pStateStr = JSON.stringify(prevState);
  		const nStateStr = JSON.stringify(nextState);
  		if(action.name !== 'SetState' && pStateStr !== nStateStr) {
  			// ignore SetState
  			history.push(nextState);
  		}
  		/*
  		console.group('State Change:')
  		console.log('Previous State:', prevState)
  		console.log('Action:', action)
  		console.log('Next State:', nextState)
  		console.groupEnd('State Change:')
  		*/
  	}
  })(app);

  const data = fetch('data.json')
  	.then(r => r.json());

  const actions = {
  	SetState: newState => () =>
  		({...newState}),
  	ButtonClick: e => state => {
  		const action = e.target.value;
  		if(action) {
  			// console.log(action)
  			switch(action) {
  				case 'ADD_STORY':
  					return ({
  						...state,
  						overlay:{
  							command:'ADD_STORY',
  							type:'actions',
  							id:state.cards.length,
  							content:{description:''}
  						}
  					})
  				case 'PLAYER_ACTION':
  					return ({
  						...state,
  						overlay:{
  							command:'PLAYER_ACTION',
  							type:'actions',
  							id:state.cards.length,
  							content:{action:'', outcome:'', result:'', roll:[[''],[''],[''],['']],  description:''}
  						}
  					})
  				case 'SET_THE_SCENE':
  					return ({
  						...state,
  						overlay:{
  							command:'SET_THE_SCENE',
  							type:'actions',
  							id:state.cards.length,
  							content:{scene:'', complication:'', alteration:'', description:''}
  						}
  					})
  				case 'PACING_MOVE':
  					return ({
  						...state,
  						overlay:{
  							command:'PACING_MOVE',
  							type:'actions',
  							id:state.cards.length,
  							content:{move:'', description:''}
  						}
  					})
  				case 'FAILURE_MOVE':
  					return ({
  						...state,
  						overlay:{
  							command:'FAILURE_MOVE',
  							type:'actions',
  							id:state.cards.length,
  							content:{move:'', description:''}
  						}
  					})
  				case 'ORACLE_YESNO':
  					return ({
  						...state,
  						overlay:{
  							command:'ORACLE_YESNO',
  							type:'oracles',
  							id:state.cards.length,
  							content:{question:'', odds:'', answer:'', description:''}
  						}
  					})
  				case 'ORACLE_HOW':
  					return ({
  						...state,
  						overlay:{
  							command:'ORACLE_HOW',
  							type:'oracles',
  							id:state.cards.length,
  							content:{question:'', answer:'', description:''}
  						}
  					})
  				case 'ACTION_FOCUS':
  					return ({
  						...state,
  						overlay:{
  							command:'ACTION_FOCUS',
  							type:'oracles',
  							id:state.cards.length,
  							content:{question:'', answer:'', description:''}
  						}
  					})
  				case 'DETAIL_FOCUS':
  					return ({
  						...state,
  						overlay:{
  							command:'DETAIL_FOCUS',
  							type:'oracles',
  							id:state.cards.length,
  							content:{question:'', answer:'', description:''}
  						}
  					})
  				case 'TOPIC_FOCUS':
  					return ({
  						...state,
  						overlay:{
  							command:'TOPIC_FOCUS',
  							type:'oracles',
  							id:state.cards.length,
  							content:{question:'', answer:'', description:''}
  						}
  					})
  				case 'GENERAL':
  					return ({
  						...state,
  						overlay:{
  							command:'GENERAL',
  							type:'oracles',
  							id:state.cards.length,
  							content:{question:'', operation:'', appearance:'', significance:'', description:''}
  						}
  					})
  				case 'RANDOM_EVENT':
  					return ({
  						...state,
  						overlay:{
  							command:'RANDOM_EVENT',
  							type:'generators',
  							id:state.cards.length,
  							content:{event:'', description:''}
  						}
  					})
  				case 'PLOT_HOOK':
  					return ({
  						...state,
  						overlay:{
  							command:'PLOT_HOOK',
  							type:'generators',
  							id:state.cards.length,
  							content:{objective:'', adversaries:'', rewards:'', description:''}
  						}
  					})
  				case 'NPC':
  					return ({
  						...state,
  						overlay:{
  							command:'NPC',
  							type:'generators',
  							id:state.cards.length,
  							content:{identity:'', goal:'', features:'', attitude:'', conversation:'', description:''}
  						}
  					})
  				case 'DUNGEON':
  					return ({
  						...state,
  						overlay:{
  							command:'DUNGEON',
  							type:'generators',
  							id:state.cards.length,
  							content:{
  								type:'',
  								location:'',
  								encounter:'',
  								object:'',
  								exits:''
  							}
  						}
  					})
  				case 'HEX':
  					return ({
  						...state,
  						overlay:{
  							command:'HEX',
  							type:'generators',
  							id:state.cards.length,
  							content:{
  								terrain:'',
  								contents:'',
  								features:'',
  								event:'',
  							}
  						}
  					})
  				case 'FEATURES':
  					return ({
  						...state,
  						overlay:{
  							command:'FEATURES',
  							type:'generators',
  							id:state.cards.length,
  							content:{features:'', description:''}
  						}
  					})
  				case 'TERRAIN':
  					return ({
  						...state,
  						overlay:{
  							command:'TERRAIN',
  							type:'generators',
  							id:state.cards.length,
  							content:{terrain:'', description:''}
  						}
  					})
  				case 'NEW_GAME':
  					return ({
  						...state,
  						overlay:{
  							command:'NEW_GAME',
  							type:'utilities',
  							id:state.cards.length,
  						}
  					})
  				case 'SAVE':
  					return ({
  						...state,
  						overlay:{
  							command:'SAVE',
  							type:'utilities',
  							id:state.cards.length,
  							content:{gameName:'', description:''}
  						}
  					})
  				case 'LOAD_GAME':
  					return ({
  						...state,
  						overlay:{
  							command:'LOAD_GAME',
  							type:'utilities',
  							id:state.cards.length,
  							content:{game:'', description:''}
  						}
  					})
  				case 'EXPORT':
  					return ({...state})
  				case 'DICE_ROLLER':
  					return ({
  						...state,
  						overlay:{
  							command:'DICE_ROLLER',
  							type:'utilities',
  							id:state.cards.length,
  							content:{type:'Fate', dice:'', quantity:'', roll:[], result:'', description:''}
  						}
  					})
  				default:
  					return ({
  						...state,
  						overlay:{
  							command:'PLAYER_ACTION',
  							type:'actions',
  							id:state.cards.length,
  							content:{action:'', result:'', description:''}
  						}
  					})
  			}
  		}
  	},
  	SelectDiceType: type => state =>
  		({ ...state, overlay:{
  			...state.overlay,
  			content:{...state.overlay.content, type}}
  		}),
  	NewGame: () => state =>
  		({...state, overlay:'', cards:[]}),
  	SaveGame: () => state => {
  		const games = JSON.parse(localStorage.getItem('games')) || [];
  		let editedGames;
  		if(state.game) {
  			if(state.game !== state.overlay?.content?.gameName) {
  				// save current game as...
  				editedGames = games.concat({name:state.overlay?.content?.gameName || state.game, cards:state.cards});
  			} else {
  				editedGames = games
  					.map(game => {
  						return state.game === game.name ? ({name:state.overlay?.content?.gameName || state.game, cards:state.cards}) : game
  					});
  			}
  		} else {
  			// save new game
  			editedGames = games.concat({name:state.overlay?.content?.gameName || state.game, cards:state.cards});
  		}
  		localStorage.setItem('games', JSON.stringify(editedGames));
  		return ({...state, overlay:'', game:state.overlay?.content?.gameName || state.game})
  	},
  	LoadGame: () => (state, actions) =>
  		({
  			...state,
  			overlay:'',
  			game:state.overlay.content.game.name,
  			cards:state.overlay.content.game.cards
  		}),
  	DeleteGame: () => (state) => {
  		let games = JSON.parse(localStorage.getItem('games')) || [];
  		games = games.filter(({name}) => name !== state.overlay.content.game.name);
  		localStorage.setItem('games', JSON.stringify(games));
  		if(!games.find(game => game.name === state.game)) {
  			return ({...state, overlay:'', game:'', cards:[]})
  		} else {
  			return ({...state, overlay:''})
  		}
  	},
  	UploadFile: file => (state, actions) => {
  		const reader = new FileReader();
  		reader.addEventListener('load', e => {
  			actions.SelectGame(JSON.parse(e.target.result));
  		});
  		reader.readAsText(file);
  	},
  	SelectGame: game => state =>
  		({ ...state, overlay:{
  			...state.overlay,
  			content:{...state.overlay.content, game}}
  		}),
  	DownloadFile: () => state => {
  		let dataStr = JSON.stringify({name:state.game, cards:state.cards});
  		let dataUri = 'data:application/json;charset=utf-8,'+encodeURIComponent(dataStr);
  		let exportFileDefaultName = `${state.game}.json`;
  		let el = document.createElement('a');
  		el.setAttribute('href', dataUri);
  		el.setAttribute('download', exportFileDefaultName);
  		el.click();
  	},
  	RollFate: () => state => {
  		const roll = generateFateRoll(state.model);
  		const result = roll.map(([_, n]) => n).reduce(sum, 0);
  		return ({
  			...state,
  			overlay:{
  				...state.overlay,
  				content: {
  					...state.overlay.content,
  					roll,
  					result
  				}
  			},
  		})
  	},
  	UpdateOverlayContent: data => state =>
  		({...state, overlay:data.content, isEdit:data.isEdit}),
  	AddCard: () => state =>
  		({
  			...state,
  			overlay:'',
  			cards:state.cards.concat({...state.overlay})
  		}),
  	EditCard: id => state =>
  		({
  			...state,
  			isEdit:false,
  			overlay:'',
  			cards:[
  				...state.cards.slice(0, id),
  				state.overlay,
  				...state.cards.slice(id + 1)]
  		}),
  	RemoveCard: id => state =>
  		({
  			...state,
  			cards:[...state.cards.slice(0, id), ...state.cards.slice(id + 1)]
  		}),
  	CloseOverlay: e => state =>
  		e.target === e.currentTarget ?
  			({...state, overlay:''}) :
  			undefined,
  	GenerateScene:() => state => {
  		const [complication, alteration] = generateScene(state.model);
  		return ({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				complication,
  				alteration
  			}
  		}})
  	},
  	GeneratePacingMove:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				move:generatePacingMove(state.model),
  			}
  		}}),
  	GenerateFailureMove:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				move:generateFailureMove(state.model),
  			}
  		}}),
  	GenerateRandomEvent:() => state => {
  		const [actionFocus, topicFocus] = generateRandomEvent(state.model);
  		return ({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				event:{
  					action: actionFocus,
  					topic: topicFocus,
  				},
  			}
  		}})
  	},
  	GeneratePlotHook:() => state => {
  		const [objective, adversaries, rewards] = generatePlotHook(state.model);
  		return ({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				objective,
  				adversaries,
  				rewards
  			}
  		}})
  	},
  	GenerateNPC:() => state => {
  		const [identity, goal, features, attitude, conversation] = generateNPC(state.model);
  		return ({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				identity,
  				goal,
  				features,
  				attitude,
  				conversation
  			}
  		}})
  	},
  	SetDungeonType:type => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				type
  			}
  		}}),
  	GenerateDungeon:() => state => {
  		const [location, encounter, object, exits] = generateDungeon(state.model);
  		const appendObj = { location, encounter, object, exits };
  		if(state.overlay.content.type === 'Entrance') {
  			appendObj.appearance = askDetailFocus(state.model).join(', ');
  			appendObj.operation = askActionFocus(state.model).join(', ');
  		}
  		return ({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				...appendObj
  			}
  		}})
  	},
  	GenerateHex:() => state => {
  		const [terrain, contents, features, event] = generateHex(state.model);
  		return ({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				terrain,
  				contents,
  				features,
  				event
  			}
  		}})
  	},
  	GenerateFeatures:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				features:generateFeatures(state.model),
  			}
  		}}),
  	GenerateTerrain:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				terrain:generateTerrain(state.model),
  			}
  		}}),
  	SetOdds:odds => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				odds,
  			}
  		}}),
  	AskYesNo:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				answer:askYesNo(state.model, state.overlay.content.odds),
  			}
  		}}),
  	AskHowMany:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				answer:askHowMany(state.model),
  			}
  		}}),
  	AskActionFocus:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				answer:askActionFocus(state.model).join(', '),
  			}
  		}}),
  	AskDetailFocus:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				answer:askDetailFocus(state.model).join(', '),
  			}
  		}}),
  	AskTopicFocus:() => state =>
  		({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				answer:askTopicFocus(state.model).join(', '),
  			}
  		}}),
  	AskGeneralQuestion:() => state => {
  		const [operation, appearance, significance] = askGeneralQuestion(state.model);
  		return ({...state, overlay:{
  			...state.overlay,
  			content: {
  				...state.overlay.content,
  				operation: operation.join(', '),
  				appearance: appearance.join(', '),
  				significance
  			}
  		}})
  	}
  };

  const initialState = {
  	overlay:'',
  	cards:[]
  };

  const getOverlayContent = (state, actions) => {
  	switch(state.overlay.command) {
  		case 'PLAYER_ACTION':
  			return PlayerActionOverlay(state, actions)
  		case 'ADD_STORY':
  			return AddStoryOverlay(state, actions)
  		case 'SET_THE_SCENE':
  			return SetTheSceneOverlay(state, actions)
  		case 'PACING_MOVE':
  			return PacingMoveOverlay(state, actions)
  		case 'FAILURE_MOVE':
  			return FailureMoveOverlay(state, actions)
  		case 'RANDOM_EVENT':
  			return RandomEventOverlay(state, actions)
  		case 'ORACLE_YESNO':
  			return OracleYesNoOverlay(state, actions)
  		case 'ORACLE_HOW':
  			return OracleHowOverlay(state, actions)
  		case 'ACTION_FOCUS':
  			return OracleActionFocusOverlay(state, actions)
  		case 'DETAIL_FOCUS':
  			return OracleDetailFocusOverlay(state, actions)
  		case 'TOPIC_FOCUS':
  			return OracleTopicFocusOverlay(state, actions)
  		case 'GENERAL':
  			return OracleGeneralOverlay(state, actions)
  		case 'PLOT_HOOK':
  			return PlotHookOverlay(state, actions)
  		case 'NPC':
  			return NPCOverlay(state, actions)
  		case 'DUNGEON':
  			return DungeonOverlay(state, actions)
  		case 'HEX':
  			return HexOverlay(state, actions)
  		case 'FEATURES':
  			return FeaturesOverlay(state, actions)
  		case 'TERRAIN':
  			return TerrainOverlay(state, actions)
  		case 'NEW_GAME':
  			return NewGameOverlay(state, actions)
  		case 'SAVE':
  			return SaveOverlay(state, actions)
  		case 'LOAD_GAME':
  			return LoadGameOverlay(state, actions)
  		case 'DICE_ROLLER':
  			return DiceRollerOverlay(state, actions)
  		default:
  			return false
  	}
  };

  const view = (state, actions) =>
  	h('main', {},
  		ControlPanel(state, actions),
  		CardDisplay(state, actions),
  		Overlay(state, actions, getOverlayContent(state, actions)),
  	);

  const init = (data, state) => () =>
  	data
  		.then(model => {
  			const initState = {model, ...state};
  			history.push(initState);
  			const allActions = historyApp(initState, actions, view, document.body);
  			window.addEventListener('keydown', createKeyCommandsExec(allActions, history));
  		})
  		.catch(e => {
  			console.log('Error:', e);
  		});

  window.addEventListener('load', init(data, initialState));

})();
